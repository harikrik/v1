<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payment_model extends CI_Model {
public function __construct() {
        parent::__construct();
        @session_start();

        //===================================================
        // Loads Paytm Authorized Files
        //===================================================
	header("Pragma: no-cache");
	header("Cache-Control: no-cache");
	header("Expires: 0");

        $this->load->library('Stack_web_gateway_paytm_kit');
	//===================================================
    }
    public function index()
    {
    }
	
	
	public function createOrder($userId, $amount){
    	$this->db
    		->select('id as id,mobile as mobile,email as email')
    		->from('user');
		$userData = $this->db->get()->row();
		
		$isnert_a = ['user_id' => $userId, 'transaction_type' => 'cr', 'amount' =>  $amount];
    	$this->db->insert('transaction_master', $isnert_a);
    	$inserted_id = $this->db->insert_id();

    	if(!$inserted_id){		
    	
    		return (object)['status' => false, 'message' => 'Sorry, please try again later'];
    	}
		
		
		
		$orderId = $inserted_id;
		$customerId = $userData->id;
		$mobileNumber = $userData->mobile;
		$email = $userData->email;		
		$txnAmt = $amount;
		
		$industryTypeId = PAYTM_INDUSTRY_TYPE_ID;
		//$callBackUrl = 'http://13.233.107.147/index.php/api/payment-status';
		$callBackUrl = PAYTM_CALLBACK_URL.$orderId;
		//$checksumHash = $this->getChecksumHash($merchantId,$orderId,$customerId,$channelId,$txnAmt,$website,$industryTypeId);
		$checksumHash = $this->stack_web_gateway_paytm_kit->getChecksumFromArray($paytmParams, PAYTM_MERCHANT_KEY);
    	if($checksumHash){
			$data = array();
			$data["merchantId"] = PAYTM_MERCHANT_MID;
			$data["orderId"] = $orderId;
			$data["customerId"] = $customerId;
			$data["mobileNumber"] = $mobileNumber;
			$data["email"] = $email;
			$data["channelId"] = PAYTM_CHANNEL_ID;
			$data["txnAmt"] = $txnAmt;
			$data["website"] = PAYTM_MERCHANT_WEBSITE;
			$data["industryTypeId"] = PAYTM_INDUSTRY_TYPE_ID;
			$data["callBackUrl"] = $callBackUrl;
			$data["checksumHash"] = $checksumHash->data;
			
    		return (object)['status' => true, 'data' => $data];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no user data found!'];
    	}
		
    }
	
	
	
	
	
    /*public function createOrder($userId, $amount){
    	$this->db
    		->select('id as id,mobile as mobile,email as email')
    		->from('user');
		$userData = $this->db->get()->row();
		
		$isnert_a = ['user_id' => $userId, 'transaction_type' => 'cr', 'amount' =>  $amount];
    	$this->db->insert('transaction_master', $isnert_a);
    	$inserted_id = $this->db->insert_id();

    	if(!$inserted_id){		
    	
    		return (object)['status' => false, 'message' => 'Sorry, please try again later'];
    	}
		
		
		$merchantId = "YBXNvN10100960775174";
		$orderId = $inserted_id;
		$customerId = $userData->id;
		$mobileNumber = $userData->mobile;
		$email = $userData->email;
		$channelId = "WAP";
		$txnAmt = $amount;
		$website = "WEBSTAGING" ;
		$industryTypeId = "Retail";
		//$callBackUrl = 'http://13.233.107.147/index.php/api/payment-status';
		$callBackUrl = 'https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID='.$orderId;
		$checksumHash = $this->getChecksumHash($merchantId,$orderId,$customerId,$channelId,$txnAmt,$website,$industryTypeId);
    	if($checksumHash){
			$data = array();
			$data["merchantId"] = $merchantId;
			$data["orderId"] = $orderId;
			$data["customerId"] = $customerId;
			$data["mobileNumber"] = $mobileNumber;
			$data["email"] = $email;
			$data["channelId"] = $channelId;
			$data["txnAmt"] = $txnAmt;
			$data["website"] = $website;
			$data["industryTypeId"] = $industryTypeId;
			$data["callBackUrl"] = $callBackUrl;
			$data["checksumHash"] = $checksumHash->data;
			
    		return (object)['status' => true, 'data' => $data];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no user data found!'];
    	}
		
    }
	
	public function getChecksumHash($merchantId,$orderId,$customerId,$channelId,$txnAmt,$website,$industryTypeId){
		//require_once(APPPATH.'libraries/encdec_paytm.php');
		//require_once(APPPATH.'libraries/config_paytm.php');
		
		//$this->load->library('encdec_paytm.php');
		//$this->load->library('config_paytm.php');
		$checkSum = "";
		// below code snippet is mandatory, so that no one can use your checksumgeneration url for other purpose .
		$findme   = 'REFUND';
		$findmepipe = '|';
		$paramList = array();
		$paramList["MID"] = $merchantId;
		$paramList["ORDER_ID"] = $orderId;
		$paramList["CUST_ID"] = $customerId;
		$paramList["INDUSTRY_TYPE_ID"] = $industryTypeId;
		$paramList["CHANNEL_ID"] = $channelId;
		$paramList["TXN_AMOUNT"] = $txnAmt;
		$paramList["WEBSITE"] = $website;
		foreach($_POST as $key=>$value)
		{  
		  $pos = strpos($value, $findme);
		  $pospipe = strpos($value, $findmepipe);
		  if ($pos === false || $pospipe === false) 
			{
				$paramList[$key] = $value;
			}
		}		  
		//Here checksum string will return by getChecksumFromArray() function.
		
		$checkSum = $this->common->getChecksumFromArray($paramList,'&PnMvgWv@Yre_bIo');
		$isValidChecksum = $this->common->verifychecksum_e($paramList, '&PnMvgWv@Yre_bIo', '1234');
		if($isValidChecksum){
			return (object)['status' => true, 'data' => $isValidChecksum];
		}else{
			return (object)['status' => false, 'message' => 'Checksum generation failed!'];
		}
		
		if($checkSum){
			return (object)['status' => true, 'data' => $checkSum];
		}else{
			return (object)['status' => false, 'message' => 'Checksum generation failed!'];
		}
		
	}*/

   
	
}