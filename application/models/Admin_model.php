<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin_model extends CI_Model {

	
	
	
	public function login($userName,$password){
		
		 $this->db
    		->select('id as id')
    		->from('admin_user')
    		->where('username', $userName)
    		->where('password', $password)
    		->where('is_active', '1')
    		->where('is_deleted', '0');				
		$data = $this->db->get()->row();		
    	if($data){				
    		return (object)['status' => true, 'data' => $data];
    	}else{
    		return (object)['status' => false, 'data' => 'Sorry, Invalid Credentials'];
    	}		
	}
	
	
	public function getLiveGames($userId){		
		
		 $this->db
    		->select('gm.id as id,gm.game_name as gameName,gm.title as title,gm.pic as pic,gm.type as type,gm.version as version,gm.map as map,gm.win_prize as winPrize,gm.entry_fee as entryFee,gm.perkill_amount as perkillAmount,
			gm.game_time as gameTime,gm.total_gamers as totalGamers,(select count(gp.id) from game_participants as gp where gm.id = gp.game_id) as joinedGamers,is_postponed as isPostponed')
    		->from('games_m as gm')			
    		->where('is_active', 1)
    		->where('is_deleted', 0)
    		->where('is_resultpublished', 0)			
			->order_by("game_time", "asc");   	
    	$data = $this->db->get()->result();	
		
		
		if($data){
				return (object)['status' => true, 'data' => $data];				
		}else{			
			return (object)['status' => false, 'message' => 'Sorry match creation failed!'];
		}
	}	
	
	public function getLiveGamesDetail($userId,$gameId){
		 $this->db
    		->select('gm.id as id,gm.game_name as gameName,gm.title as title,gm.pic as pic,gm.type as type,gm.version as version,gm.map as map,gm.win_prize as winPrize,gm.entry_fee as entryFee,gm.perkill_amount as perkillAmount,
			gm.game_time as gameTime,gm.total_gamers as totalGamers,(select count(gp.id) from game_participants as gp where gm.id = gp.game_id) as joinedGamers,is_postponed as isPostponed')
    		->from('games_m as gm')	
			->where('id', $gameId)
    		->where('is_active', 1)
    		->where('is_deleted', 0);
    		//->where('is_completed', 0);
			
		$gameData = $this->db->get()->row();		
		$participantData = array();						
		$this->db
				 ->select('id as id,gamer_id as gamerId,pubg_name as pubgName,player_unique_id as playerUniqueid,kill_number as killNumber,kill_amount as killAmount,
							is_chickendinner as isChickendinner,total_amount as totalAmount,screenshot as screenshot,is_participated as isParticipated')
				 ->from('game_participants')
				 ->where('game_id', $gameId);				 
		$participantData =  $this->db->get()->result();
		if($gameData){
			if($participantData){
				return (object)['status' => true, 'gameData' => $gameData, 'participantData' => $participantData];
			}else{
				return (object)['status' => true, 'gameData' => $gameData];
			}
		}else{
			return (object)['status' => false, 'message' => 'No details found for this match'];
			
		}		
	}
	
	
	
	
	
	
	
	public function createMatch($userId,$gameData = array()){
		date_default_timezone_set('Asia/Kolkata'); 
		
		$createdTime = date("Y-m-d H:i:s");	
		$type = "";
		$version = "";
		$gameName = $gameData->gameName;
		$title = $gameData->title;
		$pic = $gameData->pic;
		if($gameName != "8 BALL POOL"){		
		$type = $gameData->type;
		$version = $gameData->version;
		}
		$map = $gameData->map;
		$winPrize = $gameData->winPrize;
		$entryFee = $gameData->entryFee;
		$perkillAmount = $gameData->perkillAmount;
		$gameTime = date("Y-m-d H:i:s",strtotime($gameData->gameTime));
		$totalGamers = $gameData->totalGamers;
		
		
		$isnert_a = ['game_name' => $gameName, 'title' => $title, 'pic' => $pic,'type' => $type,
			        'version' => $version, 'map' => $map, 'win_prize' => $winPrize,'entry_fee' => $entryFee,
				    'perkill_amount' => $perkillAmount, 'created_time' => $createdTime, 'game_time' => $gameTime,'total_gamers' => $totalGamers];		
		$this->db->insert('games_m', $isnert_a);
		$inserted_id = $this->db->insert_id();
		if($inserted_id){
			return (object)['status' => true, 'data' => "match created successfully"];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry match creation failed!'];
    	}
    		
    }
	
	public function updateMatchResult($userId,$gameId,$participantData){
		
		if($participantData){			 
			 for($i = 0 ; $i< sizeof($participantData); $i++){
				
				 $id = $participantData[$i]->id;
				 $gameId = $participantData[$i]->gameId;
				 $gamerId = $participantData[$i]->gamerId;
				 $killNumber = $participantData[$i]->killNumber;
				 $killAmount = $participantData[$i]->killAmount;
				 $isChickendinner = $participantData[$i]->isChickendinner;
				 $totalAmount = $participantData[$i]->totalAmount;	 
				
				$this->db
					 ->where('id', $id)
					 ->set('kill_number', $killNumber)
					 ->set('kill_amount', $killAmount)
					 ->set('is_chickendinner', $isChickendinner)
					 ->set('total_amount', $totalAmount)
					 ->update('game_participants');
				if((int)$totalAmount > 0 ){	 
					$this->common->updateUserCoin($gameId,$gamerId,$totalAmount);
				}
				 
			 }
			 
			$this->db
				 ->where('id', $gameId)
				 ->set('is_completed', '1')
				 ->set('is_resultpublished', '1')				 
				 ->update('games_m'); 
			return (object)['status' => true, 'data' => "match result updated successfully"]; 
		}else{
			return (object)['status' => false, 'message' => 'Sorry Update result failed!'];
		}	 
		
		
	}
	
	public function cancelMatch($userId,$gameId){
		
		$this->db
			 ->where('id', $gameId)
			 ->set('is_active', '0')
			 ->set('is_completed', '1')
			 ->set('is_resultpublished', '1')				 
			 ->update('games_m'); 
		$update_res = $this->db->affected_rows();
		
    	if($update_res){
			$this->db				
				 ->select('entry_fee as entryFee')	
				  ->from('games_m')
				 ->where('id', $gameId);
				 
			$data = $this->db->get()->row();
			$gameFee = $data->entryFee;
			if((int)$gameFee > 0){
				$this->db
					 ->select('gamer_id as gamerId')
					 ->from('game_participants')
					 ->where('game_id', $gameId);
				$result_list =  $this->db->get()->result();
				if($result_list){
					for($i = 0 ; $i< sizeof($result_list); $i++){
						 $gamerId = $result_list[$i]->gamerId;						 
						 $this->common->updateUserBonusCoin($gameId,$gamerId,$gameFee);
					 }
				}
				
			}
			
			
			
		}else{
			return (object)['status' => false, 'message' => 'Sorry match cancellation failed!'];
		}
				 
				 
	}
	
	public function cancelMatchNew($userId,$gameId){
		
		
		$participantCount = 0;
		$this->db
			 ->select('entry_fee as entryFee,total_gamers as totalGamers')
			 ->from('games_m')
			 ->where('id', $gameId);
			 
		$data = $this->db->get()->row();
		$gameFee = $data->entryFee;	 
		$totalGamers = $data->totalGamers;
		
		$this->db
			 ->where('id', $gameId)
			 ->set('is_active', '0')
			 ->set('is_completed', '1')
			 ->set('is_resultpublished', '1')				 
			 ->update('games_m'); 
		$update_res = $this->db->affected_rows();
		
    	if($update_res){		
			
			if((int)$gameFee > 0){
				$this->db
					 ->select('gamer_id as gamerId')
					 ->from('game_participants')
					 ->where('game_id', $gameId);
				$result_list =  $this->db->get()->result();
				
				
				if($result_list){
					$participantCount = sizeof($result_list);					
					if($totalGamers == 2){
						if($participantCount > 0){ 							
							if($participantCount == 1){
								for($i = 0 ; $i< sizeof($result_list); $i++){
								$gamerId = $result_list[$i]->gamerId;						 
								$this->common->initiateRefund($gameId,$gamerId);
								
								}					
								return (object)['status' => true, 'message' => 'match cancelled'];
							}
							elseif($participantCount == 2){
								for($i = 0 ; $i< sizeof($result_list); $i++){
								$gamerId = $result_list[$i]->gamerId;				
								
								$this->common->updateUserBonusCoin($gameId,$gamerId,$gameFee);								
								}
							return (object)['status' => true, 'message' => 'match cancelled'];
							
							}							
						}else{
							return (object)['status' => true, 'message' => 'match cancelled'];
						}				
					}else{
						
						for($i = 0 ; $i< sizeof($result_list); $i++){
						 $gamerId = $result_list[$i]->gamerId;	
						 $this->common->initiateRefund($gameId,$gamerId);
						 //$this->common->updateUserBonusCoin($gameId,$gamerId,$gameFee);
						 
						}
						return (object)['status' => true, 'message' => 'match cancelled'];
						
					}
				
				}else{
					
					return (object)['status' => true, 'message' => 'match cancelled'];
				}				
				
			}			
			
		}else{
			return (object)['status' => false, 'message' => 'Sorry match cancellation failed!'];
		}			 
	}
	
	
	
}