<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payout_model extends CI_Model {

	
	
	
	public function aboutUs($userId){
    	$this->db
    		->select('about as about')
    		->from('app_about'); 		

    	$data = $this->db->get()->row();
    	if($data){
    		return (object)['status' => true, 'data' => $data];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no user data found!'];
    	}
    }
	
	
	public function generateToken(){	
		$token ="";
	    $tokenData = $this->common->payoutTokenGenerator();
		if($tokenData != "error"){
			$token = $tokenData;
		}else{
			return (object)['status' => false, 'message' => 'Sorry token gen failed'];
		}	
		
		
		
		
	}
	
	
	
	public function addBenificiery($userId,$accountHolderName,$accountNumber,$ifsc){
		$tokenData = $this->common->payoutTokenGenerator();
		if($tokenData != "error"){
			$token = $tokenData;
		}else{
			return (object)['status' => false, 'message' => 'Sorry token gen failed'];
		}		
		
		
		
		
		
		$this->db
    		->select('first_name as firstName, mobile as mobileNumber,email as email,referral_code as benificiaryId')
    		->from('user')
    		->where('is_active', 1)
    		->where('is_delete', 0)
    		->where('id', $userId);
    	$data = $this->db->get()->row();	
		$mob =	$data->mobileNumber;		
		$nameAtBank = $accountHolderName;
		/*
		$resp = $this->common->validateBankDetails($token,$mob,$accountHolderName,$accountNumber,$ifsc);
		$nameAtBank = "";
		if($resp->status){
			
			$json_obj = json_decode($resp->data);
			$status = $json_obj->status;
			if($status == "SUCCESS"){
				$accntdata = $json_obj->data;
				if($accntdata->isValidAccount == "YES"){
					$nameAtBank = $accntdata->nameAtBank;					
				}else{					
					return (object)['status' => false, 'message' => "Invalid Bank Details"];
				}
			}else{
				return (object)['status' => false, 'message' => $json_obj->message];
			}
		}else{
			return (object)['status' => false, 'message' => "Something went wrong. Try again"];
		}	*/	
		
		
		
		$response = $this->common->validateToken($token);	
		if($response){			
		$user = array();
		$benId = (string)$userId."_".$data->benificiaryId;
		$user["beneId"] = $benId;
		$user["name"]=$nameAtBank;		
		$user["email"]=$data->email;		
		$user["bankAccount"]=$accountNumber;
		$user["ifsc"]=$ifsc;		
		$user["phone"]=$data->mobileNumber;
		$response = $this->common->addPayoutBenificery($token,$user);
				
		if($response){
			$json_obj = json_decode($response);
			$status = $json_obj->status;
			$message = $json_obj->message;
			
			if($status == "SUCCESS"){
				
				$this->db
					->where('id', $userId)
					->set('ben_id', $benId)
					->update('user');

    	$update_res = $this->db->affected_rows();
    	if($update_res){
			return (object)['status' => true, 'data' => $message];
		}else{
			
			return (object)['status' => false, 'message' => $message];
		}
				
				
				
			}else{
				
				return (object)['status' => false, 'message' => $message];
			}
			
			 
		}else{
			return (object)['status' => false, 'message' => 'Adding benificiary failed'];
		}
		
		
		}else{
			return "false";
		}
		
		
		
	}
	

	
}