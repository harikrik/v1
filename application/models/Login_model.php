<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login_model extends CI_Model {

	/**
	 * function to signup new mobile number
	 * @param  [type] $mobileNumber [description]
	 * @param  [type] $otp          [description]
	 * @return [type]               [description]
	 */
    public function signup($mobileNumber, $otp){
    	$isnert_a = ['mobile' => $mobileNumber, 'otp' => $otp];
    	$this->db->insert('user', $isnert_a);
    	$inserted_id = $this->db->insert_id();

    	if($inserted_id){
			$resp = $this->sendOtp($mobileNumber,$otp);			
    		return (object)['status' => true, 'message' => 'otp generated'];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry, please try again later'];
    	}
		
    }

    /**
     * checking entered mobile number is already registered in the application
     * @param  [type] $mobileNumber [description]
     * @return [type]               [description]
     */
    public function checkMobile($mobileNumber){
    	$this->db
    		->select('*')
    		->from('user')
    		->where('mobile', $mobileNumber);

    	$data = $this->db->get()->row();

    	if($data){
    		// return ['error' => 'aready available', 'status' => false, 'message' => 'Sorry! you entered mobile number is already used'];
    		return $this->loginWithOtp($data->id,$mobileNumber);
    	}else{
    		return ['status' => true, 'message' => 'Mobile number is valid'];
    	}
    }

    public function loginWithOtp($userId,$mobile){
    	$otp = $this->common->otpGenerator(4);

    	$this->db
    		->where('id', $userId)
    		->set('otp', $otp)
    		->update('user');

    	$update_res = $this->db->affected_rows();
    	if($update_res){
			$resp = $this->sendOtp($mobile,$otp);
    		return ['error' => 'login otp generated', 'status' => true, 'message' => 'Otp Generated'];
    	}else{
    		return ['status' => false, 'message' => 'Sorry some error occured! Please contact our customer support'];
    	}

    }

    /**
     * return a message after checking the entered mobile number and otp is valid
     * @param  [type] $otp          [description]
     * @param  [type] $mobileNumber [description]
     * @return [type]               [description]
     */
    public function validateOtp($otp, $mobileNumber){
    	$this->db
    		->select('*')
    		->from('user')
    		->where('otp', $otp)
    		->where('mobile', $mobileNumber);

    	$data = $this->db->get()->row();
        
    	if($data){
			$update_a = ['otp' => ''];
			$this->db
    		->where('id', $data->id)
    		->update('user', $update_a);

    	$update_res = $this->db->affected_rows();
    		return (object)['status' => true, 'message' => 'Otp is valid', 'user_id' => $data->id , 'is_active' => $data->is_active ];
    	}else{
    		return (object)['status' => false, 'message' => 'Please enter a valid otp'];
    	}
    }

    /**
     * Activating account and return success and failure messages
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function activateAccount($userId){
		date_default_timezone_set('Asia/Kolkata');
		$dateNow = date("Y-m-d H:i:s");
    	//$update_a = ['is_active' => 1, 'otp' => '', 'first_name' => 'user-'.$userId];
		$update_a = ['is_active' => 1,'created' => $dateNow];
    	$this->db
    		->where('id', $userId)
    		->update('user', $update_a);

    	$update_res = $this->db->affected_rows();
    	if($update_res){
    		return (object)['status' => true, 'message' => 'Account activated', 'userId' => $userId, 'firstName' => 'user-'.$userId];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry account is not activated'];
    	}
    }
	
	function resetOtp($userId){
    	
    	$this->db
    		->where('id', $userId)
    		->update('user', $update_a);

    	$update_res = $this->db->affected_rows();
    	if($update_res){
    		return (object)['status' => true, 'message' => 'Account activated', 'userId' => $userId, 'firstName' => 'user-'.$userId];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry account is not activated'];
    	}
    }

    public function updateProfile($update_a, $userId){
		
		
    	$this->db
    		->where('id', $userId)
    		->update('user', $update_a);

    	$update_res = $this->db->affected_rows();
    	if($update_res){
			$timelineData = array();
			$timelineData['userId'] = $userId;
			$timelineData['type'] = "cr";
			$timelineData['detail'] = "Signup Bonus";
			$timelineData['amt'] = $update_a['bonus_coins'];
			$this->common->addToTimeline($timelineData);
    		return (object)['status' => true, 'message' => 'Profile updated!'];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry some error occured, please contact our customer support'];
    	}
    }

    /**
     * return user data with respect to the userId entered
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function getProfile($userId){	
		
		
    	$this->db
    		->select('first_name as firstName, pubg_name as pubgName, mobile as mobileNumber,coins as coins,bonus_coins as bonusCoins, email, id as userId')
    		->from('user')
    		->where('is_active', 1)
    		->where('is_delete', 0)
    		->where('id', $userId);

    	$data = $this->db->get()->row();
		
		
		$this->db
    		->select('pubg_name as pubgName, cod_name as codName,8ball_name as eightballName, 8ball_id as eightballId')
    		->from('user')
    		->where('is_active', 1)
    		->where('is_delete', 0)
    		->where('id', $userId);

    	$playerData = $this->db->get()->row();
		$playerId = null;
		$pubgName = $playerData->pubgName;
		$codName = $playerData->codName;
		$eightballName = $playerData->eightballName;
		$eightballId = $playerData->eightballId;
		
		$playerDataArr = array(
     array(
         'tag' => 'PUBG NAME',
         'playerName' => $pubgName,
		 'isString' => '1'
     ),
     array(
         'tag' => 'COD NAME',
         'playerName' => $codName,
		 'isString' => '1'
     ),
     array(
        'tag' => '8 BALL POOL NAME',
         'playerName' => $eightballName,
		 'isString' => '1'		 
     ),
     array(
        'tag' => '8 BALL POOL ID',
         'playerName' => $eightballId,
		 'isString' => '0'
         
     )		
	 
	 
);
		
		
		

		
		/*$this->db
    		->select( 'count(id) as joined, sum(kill_number) as totalKills, sum(total_amount) as totalEarnings')
			->group_by('gamer_id')
			->from('game_participants')    		
    		->where('gamer_id', $userId)*/
			
		$query = $this->db
					  ->query('SELECT * FROM game_participants as gp join games_m as gm on gm.id = gp.game_id where gm.is_resultpublished = 1 and gp.gamer_id = '.$userId.' group by gp.game_id');

		$countRows = $query->num_rows();	
		//echo($countRows);	
		/* $this->db
    		->select('count(gp.id) as joined, sum(gp.kill_number) as totalKills, sum(gp.total_amount) as totalEarnings')
    		->from('game_participants as gp')
			->group_by('gp.gamer_id')
			->join('games_m as gm' , 'gm.id = gp.game_id')
    		->where('gm.is_active', 1)
    		->where('gm.is_deleted', 0)
    		->where('gm.is_completed', 0)
			->where('gm.is_resultpublished', 0)
			->where('gp.gamer_id', $userId);*/

		$this->db
    		->select('sum(gp.total_amount) as totalEarnings')
			->from('game_participants as gp')			
			->where ('gp.gamer_id',$userId);
		$totalEarnings = $this->db->get()->row();	
		$totalEarnings = $totalEarnings->totalEarnings;	
		
		$this->db
    		->select('gp.id as id')
			->from('game_participants as gp')
			->join('games_m as gm','gm.id = gp.game_id')
			->where('gm.is_active', 1)
    		->where('gm.is_deleted', 0)
    		->where('gm.is_completed', 1)
			->where('gm.is_resultpublished', 1)
			->where ('gp.gamer_id',$userId)
			->group_by('gp.game_id');
		$joinedMatches = $this->db->get()->num_rows();	
		
		$data_stat = [];
		$data_stat['joined'] = $joinedMatches;
		$data_stat['totalKills'] = 0;
		$data_stat['totalEarnings'] = $totalEarnings;
	
		
    	if($data){
    		return (object)['status' => true, 'data' => $data,'playerData' => $playerDataArr, 'stat' => $data_stat];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no user data found!'];
    	}
    }
	
	 public function updatePubgName($userId,$playerDetails = array()){
		 $pubgName = "";
		 $codName = "";
		 $eightballName = "";
		 $eightballId = "";
		 
		 for($i = 0 ; $i< sizeof($playerDetails); $i++){
			 $tag = $playerDetails[$i]->tag;
			 $data = $playerDetails[$i]->playerName;
			 if($tag == "PUBG NAME"){
				$pubgName = $data;
			 }elseif($tag == "COD NAME"){
				 $codName = $data;
			 }elseif($tag == "8 BALL POOL NAME"){
				 $eightballName = $data;
			 }elseif($tag == "8 BALL POOL ID"){
				 $eightballId = $data;
			 }
		 }
		 
		if($pubgName == null){
			 $pubgName = "";
		}
		if($codName == null){
			 $codName = "";
		}
		if($eightballName == null){
			 $eightballName = "";
		}
		if($eightballId == null){
			 $eightballId = "";
		}
		
		 $this->db
		      ->set('pubg_name', $pubgName)
		      ->set('cod_name', $codName)
		      ->set('8ball_name', $eightballName)
		      ->set('8ball_id', $eightballId)
    		->where('id', $userId)
    		->update('user');

    	$update_res = $this->db->affected_rows();
    	
    	return (object)['status' => true, 'message' => 'Profile updated!'];
    	
		 
		 
	 }
	 
	 
	 
	 public function getReferalCode($userId){
    	$this->db
    		->select('referral_code as referralCode')
    		->from('user')
			->where('id', $userId);	

    	$data = $this->db->get()->row();
		$referalCode = $data->referralCode;
		$this->db
    		->select('message as referralMessage,referal_sending_message as referalSendingMessage')
    		->from('referal_crieria');	
		$msg = 	$this->db->get()->row();
		$referralMessage = $msg->referralMessage;
		$referalSendingMessage = $msg->referalSendingMessage;

    	if($referalCode){
    		return (object)['status' => true, 'referralCode' => $referalCode, 'referralMessage' => $referralMessage, 'referalSendingMessage' => $referalSendingMessage." ".$referalCode];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no user data found!'];
    	}
    }
	
	
	public function aboutUs($userId){
    	$this->db
    		->select('about as about')
    		->from('app_about');
    		

    	$data = $this->db->get()->row();
		
		

    	if($data){
    		return (object)['status' => true, 'data' => $data];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no user data found!'];
    	}
    }
	
	public function resendOtp($mobile){
		
		
		$this->db
    		->select('otp as otp')
    		->from('user')
    		->where('mobile', $mobile);
		$data = $this->db->get()->row();
		if(!$data){
			return (object)['status' => false, 'message' => 'Sorry no user data found!'];
		}
		
		$otp = $data->otp;
		
		
		$status =  $this->sendOtp($mobile,$otp);
		$status_s = $status->status;
		
        if($status_s){
    		return (object)['status' => true, 'data' => 'Otp resend successfully'];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no user data found!'];
    	}
	}
	
	
	 public function deleteUserFirebaseDetails($userId,$deviceId){
		 $this->db
    		->select('user_id as userId,device_id as deviceId,is_active as isActive,is_deleted as isDeleted')
    		->from('notification_m')
    		->where('user_id', $userId)
    		->where('device_id', $deviceId);
		 $data = $this->db->get()->row();
		if($data){
			$this->db
				->where('user_id', $userId)
				->where('device_id', $deviceId)    		
				->set('is_active', 0)
				->set('is_deleted', 1)
				->update('notification_m');
			$update_res = $this->db->affected_rows();	
			if($update_res){
				return (object)['status' => true, 'data' => 'token removed successfully'];
			}else{
    		return (object)['status' => false, 'message' => 'Sorry, please try again later'];
			}	
			
		}else{
			
			return (object)['status' => false, 'message' => 'Sorry, please try again later'];
		}
	 }
	
	
	public function addUserFirebaseDetails($userId,$token,$deviceId,$deviceType){
		
		$this->db
    		->select('user_id as userId,device_id as deviceId,is_active as isActive,is_deleted as isDeleted')
    		->from('notification_m')
    		->where('user_id', $userId)
    		->where('device_id', $deviceId);
		$data = $this->db->get()->row();
		if($data){
			$isActive = $data->isActive;
			$isDeleted = $data->isDeleted;
			$this->db
				->where('user_id', $userId)
				->where('device_id', $deviceId)
				->set('firebase_token', $token)
				->set('is_active', 1)
				->set('is_deleted', 0)
				->update('notification_m');

			$update_res = $this->db->affected_rows();
			if($update_res){
				return (object)['status' => true, 'data' => 'token updated successfully'];
			}else{
    		return (object)['status' => false, 'message' => 'Sorry, please try again later'];
			}
		}else{
		
		$isnert_a = ['user_id' => $userId, 'firebase_token' => $token, 'device_id' => $deviceId, 'device_type' => $deviceType];
    	$this->db->insert('notification_m', $isnert_a);
    	$inserted_id = $this->db->insert_id();
    	if($inserted_id){					
    		return (object)['status' => true, 'data' => 'token added successfully'];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry, please try again later'];
    	}
		}
		
	}
	
	public function readJson()
    {
       	$json_obj = json_decode('');
        $postdata = file_get_contents("php://input");
        $json_obj=json_decode($postdata);
        return $json_obj;
    }
	
	public function sendNotification($title,$message){	

			$isnert_a = ['title' => $title, 'message' => $message];
			$this->db->insert('game_notification', $isnert_a);
			$inserted_id = $this->db->insert_id();
			if($inserted_id){					
				define( 'API_ACCESS_KEY', 'AAAAQ68COMU:APA91bEzRjMhZ1GmuTWmyC6CfPeamSHXOZTzIR-w6CEdTFzgqwIBnsXyduIm5fIwGnmplaKpqiskKc3u2A7_1cBr5z7BHU-44E0aZehBKVS_SQySBqMpy1CjK2LgqMXOPtyVDW81Mrdv' );
				$this->db
					->select('user_id as userId,firebase_token as token')
					->from('notification_m')
					->where('is_active', 1)
					->where('is_deleted', 0);
				$data_not = $this->db->get()->result();
				if($data_not){				
					for($i = 0 ; $i< sizeof($data_not); $i++){
						$data = array("to" => $data_not[$i]->token,
						"notification" => array( "title" => $title, "body" => $message ));                                                                    
						$data_string = json_encode($data); 
						//echo "The Json Data : ".$data_string; 
						$headers = array
						(
							 'Authorization: key=' . API_ACCESS_KEY, 
							 'Content-Type: application/json'
						);                                                                               
																																			 
						$ch = curl_init();  

						curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );                                                                  
						curl_setopt( $ch,CURLOPT_POST, true );  
						curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string);                                                                  
																																			 
						$result = curl_exec($ch);
						curl_close ($ch);
	
					
				}
				 return (object)['status' => true, 'data' => "notification send successfully"];
			}else{
				return (object)['status' => false, 'message' => 'no data found'];
			
			}

			
	}else{
		return (object)['status' => false, 'message' => 'something went wrong!'];
		
	}
	}
	
	
	public function sendNotificationSpecificAudience($title,$message,$gameId){	

			
							
				define( 'API_ACCESS_KEY', 'AAAAQ68COMU:APA91bEzRjMhZ1GmuTWmyC6CfPeamSHXOZTzIR-w6CEdTFzgqwIBnsXyduIm5fIwGnmplaKpqiskKc3u2A7_1cBr5z7BHU-44E0aZehBKVS_SQySBqMpy1CjK2LgqMXOPtyVDW81Mrdv' );
				
				
				
				
				$this->db
    		->select('nm.user_id as userId,nm.firebase_token as token')
			->from('game_participants as gp')
			->join('notification_m as nm','nm.user_id = gp.gamer_id')
			->where('nm.is_active', 1)
    		->where('nm.is_deleted', 0)
    		->where('gp.game_id', $gameId)			
			->group_by('gp.gamer_id');
				
				
				$data_not = $this->db->get()->result();			
				
				if($data_not){				
					for($i = 0 ; $i< sizeof($data_not); $i++){
						$data = array("to" => $data_not[$i]->token,
						"notification" => array( "title" => $title, "body" => $message ));                                                                    
						$data_string = json_encode($data); 
						//echo "The Json Data : ".$data_string; 
						$headers = array
						(
							 'Authorization: key=' . API_ACCESS_KEY, 
							 'Content-Type: application/json'
						);                                                                               
																																			 
						$ch = curl_init();  

						curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );                                                                  
						curl_setopt( $ch,CURLOPT_POST, true );  
						curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string);                                                                  
																																			 
						$result = curl_exec($ch);
						curl_close ($ch);
	
					
				}
				 return (object)['status' => true, 'data' => "notification send successfully"];
			}else{
				return (object)['status' => false, 'message' => 'no data found'];
			
			}

			
	
		
	}
	
	
	
	public function getAppState(){
		
		$this->db
    		->select('version_code as versionCode,is_maintainance_break as isMaintainance')
    		->from('app_status')
			->order_by("id", "desc")
			->limit(1);
		$data = $this->db->get()->row();		
		if($data){
			return (object)['status' => true, 'data' => $data];
		}else{
			return (object)['status' => false, 'message' => 'something went wrong!'];
		}
		
	}
	
	public function sendOtp($mobile,$otp){
		
		$curl = curl_init();
        $url = "http://2factor.in/API/V1/3460c6c7-acf6-11e9-ade6-0200cd936042/SMS/".$mobile."/".$otp."/ch_prod";
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_HTTPHEADER => array(
			"content-type: application/x-www-form-urlencoded"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

		if ($err) {
		  return (object)['status' => false, 'data' => $err];
		} else {
		   return (object)['status' => true, 'data' => $response];
		}
	}
	
}