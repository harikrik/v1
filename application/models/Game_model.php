<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Game_model extends CI_Model {



public function uploadFile($userId,$gameId,$image){
	date_default_timezone_set('Asia/Kolkata'); 
	
	$dateNow = date("Y-m-d H:i:s");	
	$timestamp = strtotime($dateNow);
	$path = "/var/www/html/v1/screenshots/";
	$picName = $gameId."_".$userId."_".$timestamp.".jpeg";
	$filename = $path.$picName;
	$this->common->base64ToImage($image, $filename);
	
	
	$this->db
    		 ->where('game_id', $gameId)
    		 ->where('gamer_id', $userId)
    		 ->set('screenshot', $picName)
    		 ->update('game_participants');
			 
	return (object)['status' => true, 'data' => $picName];		 
	
}


 public function getLiveGames($userId){
	 /*$this->db
    		->select('gm.id as id,gm.game_name as gameName,gm.title as title,gm.pic as pic,gm.type as type,gm.version as version,gm.map as map,gm.win_prize as winPrize,gm.entry_fee as entryFee,gm.perkill_amount as perkillAmount,
			gm.game_time as gameTime,gm.total_gamers as totalGamers,(select count(gp.id) from game_participants as gp where gm.id = gp.game_id) as joinedGamers,is_postponed as isPostponed')
    		->from('games_m as gm')			
			->where('game_name', "PUBG")
    		->where('is_active', 1)
    		->where('is_deleted', 0)
    		->where('is_completed', 0)			
			->order_by("id", "desc");
    	$data = $this->db->get()->result(); // change hari*/
		
		/*$this->db
    		->select('gm.id as id,gm.game_name as gameName,gm.title as title,gm.pic as pic,gm.type as type,gm.version as version,gm.map as map,
			gm.win_prize as winPrize,gm.entry_fee as entryFee,gm.perkill_amount as perkillAmount,gm.game_time as gameTime,gm.total_gamers as totalGamers,
			(select count(gp.id) from game_participants as gp where gm.id = gp.game_id) as joinedGamers,is_postponed as isPostponed')
    		->from('games_m as gm')		
			->join('game_participants as gp','select count(gp.id) from game_participants as gp where gm.id = gp.game_id != gm.total_gamers')
			->where('gm.game_name', "PUBG")
    		->where('gm.is_active', 1)
    		->where('gm.is_deleted', 0)
    		->where('gm.is_completed', 0)
			->where('gm.is_resultpublished', 0)
			->group_by('gm.id');*/
		
		$sql = "select gm.id as id,gm.game_name as gameName,gm.title as title,gm.pic as pic,gm.type as type,gm.version as version,gm.map as map,
					gm.win_prize as winPrize,gm.entry_fee as entryFee,gm.perkill_amount as perkillAmount,gm.game_time as gameTime,gm.total_gamers as totalGamers,
					(select count(gp.id) from game_participants as gp where gm.id = gp.game_id) as joinedGamers,is_postponed as isPostponed from games_m as gm 
					join game_participants as gp on (select count(gp.id) from game_participants as gp where gm.id = gp.game_id) != gm.total_gamers
					where gm.game_name = 'PUBG' and gm.is_active = 1 and gm.is_deleted = 0 and gm.is_completed = 0 group by gm.id order by gm.game_time asc";
		$query = $this->db->query($sql);		
    	$data = $query->result();
		
 if($data){
	 
		$this->db
    		->select('pubg_name as pubgName,cod_name as codName,8ball_name as eightballName,8ball_id as eightballId')
			->from('user')
			->where('id', $userId);
    		$pubgName = "";
    		$codName = "";
    		$eightballName = "";
    		$eightballId = "";
			$dataPlayer = $this->db->get()->row();
			$pubgName = $dataPlayer->pubgName;
			$codName = $dataPlayer->codName;
			$eightballName = $dataPlayer->eightballName;
			$eightballId = $dataPlayer->eightballId;
	 
	 
		for($i = 0 ; $i< sizeof($data); $i++){
			
			$playerData = array();
			$gameName = $data[$i]->gameName;
			if($gameName == "PUBG" or $gameName == "PUBG PVP"){
			$playerData =	array(
				 array(
					 'tag' => 'PUBG NAME',
					 'playerName' => $pubgName,
					 'isString' => '1'
				 )
			);
				
			}elseif($gameName == "COD PVP"){
				$playerData =	array(
				 array(
					 'tag' => 'COD NAME',
					 'playerName' => $codName,
					 'isString' => '1'
				 ) 
				 
			);
			}elseif($gameName == "8 BALL POOL"){
				$playerData =	array(
				 array(
					 'tag' => '8 BALL POOL NAME',
					 'playerName' => $eightballName,
					 'isString' => '1'
				 ),
				 array(
					 'tag' => '8 BALL POOL ID',
					 'playerName' => $eightballId,
					 'isString' => '1'
				 )		 
				 
			);
			}	
			
			$data[$i]->playerdata = $playerData;
			
			
		}
 }
		
		/*print_r($data);
		for($i = 0 ; $i< sizeof($data); $i++){
			if(!empty($data[$i])){
			$totalgamers = (int)$data->totalGamers;
			$joinedgamers = (int)$data->joinedGamers;
			if($totalgamers == $joinedgamers){
					//unset($data[$i]);
					array_splice($data,$data[$i],1);
					//$data = array_values($data);
			    }
			}	
			
			}*/
		
		
		
		
		
	/*	for($i = 0 ; $i< sizeof($data); $i++){
			//echo($data[$i]->id);
			//echo("*");
	 $this->db
		   ->select('COUNT(id) as joinedGamers')
	       ->from('game_participants')
		   ->where('game_id',$data[$i]->id); 
	  $res = $this->db->get()->row();
	  $joinGamers = 	$res->joinedGamers;
	  $data[$i]->joinedGamers = $joinGamers;
	  if((int)$joinGamers == (int)$data[$i]->totalGamers){ 
	  // to remove live game list if match is full
		  //unset($data[$i]);
		  
	  }
	  
		}*/
		
		

		$this->db
    		 ->select('pubg_name as pubgName,coins as coins')
			 ->from('user')
			 ->where('id',$userId);
		$pubgname = $this->db->get()->row();         
		
		
		$this->db
    		 ->select('id as id, game_name as gameName, game_image as gameImage')
			 ->from('game_list');
			 
		$gameList = $this->db->get()->result();
		
		
    	
    	if($data or $gameList){
    		
		
				//return (object)['status' => true, 'data' => $data->result(), 'pubgName' => $pubgname->pubgName, 'coins' => $pubgname->coins];
				return (object)['status' => true, 'data' => $data, 'pubgName' => $pubgname->pubgName, 'coins' => $pubgname->coins, 'gameList' => $gameList]; //change hari
				//return (object)['status' => true, 'data' => $data, 'pubgName' => $pubgname->pubgName, 'coins' => $pubgname->coins];
				
			
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no game data found!'];
    	}	 
 }
 
 
 
 
 public function getLiveGamesByName($userId,$gameName){
	/* $this->db
    		->select('gm.id as id,gm.game_name as gameName,gm.title as title,gm.pic as pic,gm.type as type,gm.version as version,gm.map as map,
			gm.win_prize as winPrize,gm.entry_fee as entryFee,gm.perkill_amount as perkillAmount,gm.game_time as gameTime,gm.total_gamers as totalGamers,
			(select count(gp.id) from game_participants as gp where gm.id = gp.game_id) as joinedGamers,is_postponed as isPostponed')
    		->from('games_m as gm')			
			->where('game_name', $gameName)
    		->where('is_active', 1)
    		->where('is_deleted', 0)
    		->where('is_completed', 0)	   			
			->order_by("id", "desc"); */
			
			$sql = "select gm.id as id,gm.game_name as gameName,gm.title as title,gm.pic as pic,gm.type as type,gm.version as version,gm.map as map,
					gm.win_prize as winPrize,gm.entry_fee as entryFee,gm.perkill_amount as perkillAmount,gm.game_time as gameTime,gm.total_gamers as totalGamers,
					(select count(gp.id) from game_participants as gp where gm.id = gp.game_id) as joinedGamers,is_postponed as isPostponed from games_m as gm 
					join game_participants as gp on (select count(gp.id) from game_participants as gp where gm.id = gp.game_id) != gm.total_gamers
					where gm.game_name = '".$gameName."' and gm.is_active = 1 and gm.is_deleted = 0 and gm.is_completed = 0 group by gm.id order by gm.game_time asc";
			$query = $this->db->query($sql);		
			
		/*$this->db
    		->select('gm.id as id,gm.game_name as gameName,gm.title as title,gm.pic as pic,gm.type as type,gm.version as version,gm.map as map,
			gm.win_prize as winPrize,gm.entry_fee as entryFee,gm.perkill_amount as perkillAmount,gm.game_time as gameTime,gm.total_gamers as totalGamers,
			(select count(gp.id) from game_participants as gp where gm.id = gp.game_id) as joinedGamers,is_postponed as isPostponed')
    		->from('games_m as gm')		
			->join('game_participants as gp','select count(gp.id) from game_participants as gp where gm.id = gp.game_id = gm.total_gamers')
			->where('gm.game_name', $gameName)
    		->where('gm.is_active', 1)
    		->where('gm.is_deleted', 0)
    		->where('gm.is_completed', 0)
			->where('gm.is_resultpublished', 0)
			->group_by('gm.id');
			//->order_by("id", "desc");	*/
			
    //	$data = $this->db->get()->row(); // change hari
    	$data = $query->result();
		if($data){
	 
		$this->db
    		->select('pubg_name as pubgName,cod_name as codName,8ball_name as eightballName,8ball_id as eightballId')
			->from('user')
			->where('id', $userId);
    		$pubgName = "";
    		$codName = "";
    		$eightballName = "";
    		$eightballId = "";
			$dataPlayer = $this->db->get()->row();
			$pubgName = $dataPlayer->pubgName;
			$codName = $dataPlayer->codName;
			$eightballName = $dataPlayer->eightballName;
			$eightballId = $dataPlayer->eightballId;
	 
	 
		for($i = 0 ; $i< sizeof($data); $i++){
			
			$playerData = array();
			$gameName = $data[$i]->gameName;
			
			if($gameName == "PUBG" or $gameName == "PUBG PVP"){
			$playerData =	array(
				 array(
					 'tag' => 'PUBG NAME',
					 'playerName' => $pubgName,
					 'isString' => '1'
				 )
			);
				
			}elseif($gameName == "COD PVP"){
				$playerData =	array(
				 array(
					 'tag' => 'COD NAME',
					 'playerName' => $codName,
					 'isString' => '1'
				 ) 
				 
			);
			}elseif($gameName == "8 BALL POOL"){
				$playerData =	array(
				 array(
					 'tag' => '8 BALL POOL NAME',
					 'playerName' => $eightballName,
					 'isString' => '1'
				 ),
				 array(
					 'tag' => '8 BALL POOL ID',
					 'playerName' => $eightballId,
					 'isString' => '1'
				 )		 
				 
			);
			}	
			
			$data[$i]->playerdata = $playerData;
			
			
		}
 }
		
	/*	if($data){		
			
		for($i = 0 ; $i< sizeof($data); $i++){
			//echo($data[$i]->id);
			//echo("*");
	 $this->db
		   ->select('COUNT(id) as joinedGamers')
	       ->from('game_participants')
		   ->where('game_id',$data[$i]->id);
		   
		   
	  $res = $this->db->get()->row();
	 
	  $joinGamers = 	$res->joinedGamers;
	  $data[$i]->joinedGamers = $joinGamers;
	
	 
	   if((int)$joinGamers == (int)$data[$i]->totalGamers){ 
	  // to remove live game list if match is full
	 // array_push($removalIndexes,$i);
	  
		// unset($data[$i]);
		// array_splice($data,$i,1);
		//array_values($data);
	  }
		}
		}*/
		
		
		
		
		
		$this->db
    		 ->select('pubg_name as pubgName,coins as coins')
			 ->from('user')
			 ->where('id',$userId);
		$pubgname = $this->db->get()->row(); 	
    	    
    	if($data){
    		
		
				//return (object)['status' => true, 'data' => $data->result(), 'pubgName' => $pubgname->pubgName, 'coins' => $pubgname->coins];
				return (object)['status' => true, 'data' => $data, 'pubgName' => $pubgname->pubgName, 'coins' => $pubgname->coins]; //change hari
				//return (object)['status' => true, 'data' => $data, 'pubgName' => $pubgname->pubgName, 'coins' => $pubgname->coins];
				
			
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no game data found!'];
    	}	 
 }
 
 
 
 
  public function getOngoingGames($userId){	 

	
	$this->db
          ->select('gp.game_id as gameId') 
		  ->from('game_participants as gp')
		  ->join('games_m as gm','gm.id = gp.game_id')
		  ->where('gp.gamer_id',$userId)
		  ->where('gm.is_active', 1)
		  ->where('gm.is_deleted', 0)
		  ->where('gm.is_completed',0)
		  ->group_by("game_id")
		  ->order_by("gm.game_time", "asc");
		  
	$result =  $this->db->get()->result();
	
	$data = array();
	
	if($result){	
	
		$dataPlayer = array();
		$pubgName = "";
    	$codName = "";
    	$eightballName = "";
    	$eightballId = "";
		$this->db
    		->select('pubg_name as pubgName,cod_name as codName,8ball_name as eightballName,8ball_id as eightballId')
			->from('user')
			->where('id', $userId);
		$dataPlayer = 	$this->db->get()->row();
		if($dataPlayer){
        $pubgName = $dataPlayer->pubgName;
		$codName = $dataPlayer->codName;
		$eightballName = $dataPlayer->eightballName;
		$eightballId = $dataPlayer->eightballId;
		}	
 
	for($i = 0 ; $i< sizeof($result); $i++){
		 $gameId = $result[$i]->gameId;
		
		 $this->db
          ->select('gm.id as id,gm.title as title,gm.game_name as gameName,gm.pic as pic,gm.type as type,gm.version as version,gm.map as map,gm.win_prize as winPrize,gm.entry_fee as entryFee,gm.perkill_amount as perkillAmount,
			gm.game_time as gameTime,gm.total_gamers as totalGamers,(select count(gp.id) from game_participants as gp where gm.id = gp.game_id) as joinedGamers,is_postponed as isPostponed')
    	->from('games_m as gm')
		->where('gm.id',$gameId)
		->where('gm.is_active', 1)
    	->where('gm.is_deleted', 0)
		->where('gm.is_completed',0)
		->order_by("gm.id", "desc");
		 $res =  $this->db->get()->row();
		 
		 if($res){
		 $gameName = $res->gameName;
		
		$playerData = array();
		
			
			if($gameName == "PUBG" or $gameName == "PUBG PVP"){
			$playerData =	array(
				 array(
					 'tag' => 'PUBG NAME',
					 'playerName' => $pubgName,
					 'isString' => '1'
				 )
			);
				
			}elseif($gameName == "COD PVP"){
				$playerData =	array(
				 array(
					 'tag' => 'COD NAME',
					 'playerName' => $codName,
					 'isString' => '1'
				 ) 
				 
			);
			}elseif($gameName == "8 BALL POOL"){
				$playerData =	array(
				 array(
					 'tag' => '8 BALL POOL NAME',
					 'playerName' => $eightballName,
					 'isString' => '1'
				 ),
				 array(
					 'tag' => '8 BALL POOL ID',
					 'playerName' => $eightballId,
					 'isString' => '1'
				 )		 
				 
			);
			}			
			$res->playerdata = $playerData;
		    array_push($data,$res);			
		 }
		 
	 }

	 
	 $this->db
    		 ->select('pubg_name as pubgName,coins as coins')
			 ->from('user')
			 ->where('id',$userId);
	$pubgname = $this->db->get()->row(); 
	 
	 
	 
	 
	  return (object)['status' => true, 'data' => $data, 'pubgName' => $pubgname->pubgName, 'coins' => $pubgname->coins];
	 
	}
	else{
		 return (object)['status' => false, 'message' => 'Sorry no game data found!'];
	}

	
    
 }
 
 
  public function getGameResult($userId){	
  
	$this->db
          ->select('gp.game_id as gameId') 
		  ->from('game_participants as gp')
		  ->join('games_m as gm','gm.id = gp.game_id')
		  ->where('gp.gamer_id',$userId)
		  //->where('gm.is_active', 1)
		  ->where('gm.is_deleted', 0)
		  ->where('gm.is_completed',1)
		  ->where('gm.is_resultpublished', 1)
		  ->order_by("gm.game_time", "desc")
		  ->group_by("gp.game_id");  

	/*$this->db
          ->select('game_id as gameId') 
		  ->from('game_participants')
		  ->where('gamer_id',$userId)
		  ->order_by("id", "desc")
		  ->group_by('game_id');*/
		  
	$result =  $this->db->get()->result();
	$datax = array();
	if($result){	
	
	for($i = 0 ; $i< sizeof($result); $i++){
		 $gameId = $result[$i]->gameId;
		 
	 $this->db
    		->select('id as id,title as title,game_name as gameName,pic as pic,type as type,version as version,map as map,win_prize as winPrize,entry_fee as entryFee,perkill_amount as perkillAmount,
			game_time as gameTime,total_gamers as totalGamers,joined_gamers as joinedGamers,is_active as status')
    		->from('games_m')
			->where('id', $gameId)
    		//->where('is_active', 1)// removed is active 1 since to add cancelled games in this api
    		->where('is_deleted', 0)
    		->where('is_completed', 1)
			->where('is_resultpublished', 1);
    	$data = $this->db->get()->row();
		if($data){
		array_push($datax,$data); 
		}
	}
	if($datax){
		return (object)['status' => true, 'data' => $datax];
	}else{
		return (object)['status' => false, 'message' => 'Sorry no game data found!'];
	}
	}else{
		return (object)['status' => false, 'message' => 'Sorry no game data found!'];
	}
    	 
 }
 
 public function getGameResultByName($userId,$gameName){
	 $this->db
    		->select('id as id,title as title,pic as pic,type as type,version as version,map as map,win_prize as winPrize,entry_fee as entryFee,perkill_amount as perkillAmount,
			game_time as gameTime,total_gamers as totalGamers,joined_gamers as joinedGamers')
    		->from('games_m')
			->where('game_name', $gameName)
    		->where('is_active', 1)
    		->where('is_deleted', 0)
    		->where('is_completed', 1)
			->where('is_resultpublished', 1)
			->order_by("id", "desc");
    	$data = $this->db->get();        		
    	if($data){
    		return (object)['status' => true, 'data' => $data->result()];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no game data found!'];
    	}	 
 }
 
  public function getGameResultDetail($userId,$gameId){
	  $this->db
		   ->select('gamer_id as gamerId,pubg_name as pubgName,kill_number as killNumber,kill_amount as killAmount,is_chickendinner as isChicken,total_amount as totalWinnings')
	       ->from('game_participants')
		   ->where('game_id', $gameId)
		   ->order_by("total_amount", "desc");
	  $data = $this->db->get();        		
    	if($data){
    		return (object)['status' => true, 'data' => $data->result()];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no game details found!'];
    	}	 	   
		   
  }
  
  
  public function withdrawFund($userId,$amount){
	   date_default_timezone_set('Asia/Kolkata'); 
	   $this->db
    		->select('datetime as datetime,mobile as mobile') 
    		->from('withdrawal_request')
    		->where('user_id', $userId)
			->order_by("id", "desc")
			->limit('1');
		$lastWithdrawDate = $this->db->get()->row();

		if($lastWithdrawDate){
		$lastWithdraw = $lastWithdrawDate->datetime;		
			$dateNow = date("Y-m-d H:i:s");		
			if( (int)(strtotime($dateNow) - strtotime($lastWithdraw)) < 86400 ){ // 86400 -> 24 hrs to seconds			
				return (object)['status' => false, 'message' => 'Your withdrawal is processed only once in 24 hours'];			
			}				
		}	
	  $this->db
    		->select('mobile as mobile,coins as coins,is_banned as isBanned')
    		->from('user')
    		->where('id', $userId);
	  $data = $this->db->get()->row();
	  $isBanned = $data->isBanned;
	if($isBanned){
		return (object)['status' => false, 'message' => 'YOU ARE BANNED FOR HACKING IN GAME! Contact cratehunt.2019@gmail.com for further proceedings'];		
	}
	  
	  $mobile = $data->mobile;
	  $coins = $data->coins;
	  
	  if((int)$amount < 20){		  
		   return (object)['status' => false, 'message' => 'Sorry Minimum withdrawal is Rs 20'];
	  }
	  
	  if((int)$coins >= (int)$amount ){
		  
		  $newBalance = (int)$coins - (int)$amount;
		  $this->db
    		 ->where('id', $userId)
    		 ->set('coins', $newBalance)
    		 ->update('user');
		$update_res = $this->db->affected_rows();		
    	if($update_res){
			$date = date("Y-m-d H:i:s");			
			$isnert_a = ['user_id' => $userId, 'amount' => $amount, 'datetime' => $date];
			$this->db->insert('withdrawal_request', $isnert_a);
			$inserted_id = $this->db->insert_id();
			if($inserted_id){
				
				$isnert_b = ['user_id' => $userId, 'transaction_type' => 'dr', 'amount' =>  $amount, 'created' =>  $date, 'status' => '1', 'withdrawal_id' => $inserted_id];
				$this->db->insert('transaction_master', $isnert_b);
				$inserted_id_new = $this->db->insert_id();
				if($inserted_id_new){
					
					$timelineData = array();
					$timelineData['userId'] = $userId;
					$timelineData['type'] = "dr";
					$timelineData['detail'] = "Withdrawal";
					$timelineData['amt'] = $amount;
					$this->common->addToTimeline($timelineData);
				    
					return (object)['status' => true, 'message' => 'Your withdrawal request for Rs '.$amount.' is initiated'];
				}else{
					 return (object)['status' => false, 'message' => 'Sorry something went wrong!'];
				}
			}
			else{
		  return (object)['status' => false, 'message' => 'Sorry something went wrong!'];
		  
	  }
		}else{
		  return (object)['status' => false, 'message' => 'Sorry something went wrong!!'];
		  
	  }		  
		  
	  }else{
		  return (object)['status' => false, 'message' => 'Sorry You don\'t have sufficient coins!'];
		  
	  }
	  
  }
	
 
 
 
 public function getUserCoins($userId){	 
	 $this->db
    		->select('coins as coins,bonus_coins as bonusCoins')
    		->from('user')
    		->where('id', $userId);    		
    	$data = $this->db->get()->row();        		
    	if($data){
    		return (object)['status' => true, 'data' => $data];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no game data found!'];
    	}	

		
 }
 
 
 public function encrypt($crypto,$config){
	 $this->common->encrypt($crypto,$config);
	 $key = hex2bin($config->item('key'));	 
	 $crypto->initialize(
        array(
                'cipher' => 'aes-256',
                'mode' => 'cbc',
				'driver' => 'openssl',
                'key' => $key
        )	
	);
	$ciphertext = $crypto->encrypt('My secret message');	
	$message = $crypto->decrypt($ciphertext);	
	return (object)['status' => true, 'data' => $ciphertext];
	 
 }
 
  public function decrypt($crypto){
	 $key = $crypto->create_key(32);
	 if($key){
		return (object)['status' => true, 'data' => $key];
	 }else{
		 return (object)['status' => false, 'message' => 'no key'];
	 }
 }
 
 
  public function getMatchNew($gameId,$userId){
	 date_default_timezone_set('Asia/Kolkata'); 
	 $packageName = "";	 
	
	 $isActiveRoom = 0;
	 $result_list = array(); 
	 $room = array();
	 $this->db
		->select('game_name as gameName')
		->from('games_m')
	    ->where('id',$gameId);
	 $gameName = $this->db->get()->row();
	 $gameName = $gameName->gameName;
	 
	 if($gameName == "PUBG"){
		 $this->db
    		->select('gamer_id as gamerId,pubg_name as playerName,screenshot as screenshot')
    		->from('game_participants')
    		->where('game_id', $gameId);
		 $result_list =  $this->db->get()->result();		
		 $query = $this->db->query('SELECT id as Id FROM game_participants where gamer_id = '.$userId.' and  game_id = '.$gameId.' group by game_id');
	     $countRows = $query->num_rows();
	     if((int)$countRows > 0 ){
				$this->db
					 ->select('is_activeroom_credential as isActiveRoom,room_id as roomId,room_pass as roomPass')
					 ->from('games_m')
					 ->where('id',$gameId);
					 
				$roomData = $this->db->get()->row();				
				$isActiveRoom = $roomData->isActiveRoom;				
				$roomId = $roomData->roomId;
				$roomPass = $roomData->roomPass;
				
				$room['tag1'] = "Room ID" ;
				$room['tag2'] = "Room Password" ;
				$room['param1'] = $roomId;
				$room['param2'] = $roomPass;
				$room['copy1'] = "1";
				$room['copy2'] = "1";				
				$room['isScreenshotNeeded'] = "0";
				
		}
		 
	 }elseif($gameName == "PUBG PVP"  or $gameName == "COD PVP") {
		  $this->db
    		->select('gamer_id as gamerId,pubg_name as playerName,screenshot as screenshot')
    		->from('game_participants')
    		->where('game_id', $gameId);
		 $result_list =  $this->db->get()->result();
		  if($result_list){			 
			 for($i = 0 ; $i< sizeof($result_list); $i++){
					$gamerId = $result_list[$i]->gamerId;
					if($gamerId != $userId){
						$result_list[$i]->playerName = "XXXXXXXXXXXXXXX" ;						
					}
		  }
		  }
		 $query = $this->db->query('SELECT id as Id FROM game_participants where gamer_id = '.$userId.' and  game_id = '.$gameId.' group by game_id');
	     $countRows = $query->num_rows();
	     if((int)$countRows > 0 ){
			 $this->db
				  ->select('game_time as gameTime')
				  ->from('games_m')
				  ->where('id',$gameId);
				  
			$gameTime = $this->db->get()->row();
			$gameTime = $gameTime->gameTime;
			$dateNow = date("Y-m-d H:i:s");
			if(strtotime($dateNow) - strtotime($gameTime) >= 0){
				
				$isActiveRoom = 1;
				$room = array();
			    $room['isActiveRoom'] = 1;
				$room['tag1'] = "Player Name" ;
				$room['copy1'] = "1";				
				$playerData = "";
				$isScreenshotNeeded = $this->common->isScreenshotNeeded($gameId,$userId);
				$room['isScreenshotNeeded'] = $isScreenshotNeeded;
				$playerData = $this->common->getPvpOpponent($userId,$gameId);
				if($playerData){
					$room['param1'] = $playerData;
				}
			}
			
		 }
		 
	 }elseif($gameName == "8 BALL POOL"){
		  $this->db
    		->select('gamer_id as gamerId,,pubg_name as playerName,player_unique_id as uniqueId,screenshot as screenshot')
    		->from('game_participants')
    		->where('game_id', $gameId);
		 $result_list =  $this->db->get()->result();
		  if($result_list){			 
			 for($i = 0 ; $i< sizeof($result_list); $i++){
					$gamerId = $result_list[$i]->gamerId;
					if($gamerId != $userId){
						$result_list[$i]->playerName = "XXXXXXXXXXXXXXX" ;						
					}
		  }
		  }
		 $query = $this->db->query('SELECT id as Id FROM game_participants where gamer_id = '.$userId.' and  game_id = '.$gameId.' group by game_id');
	     $countRows = $query->num_rows();
	     if((int)$countRows > 0 ){
			 $this->db
				  ->select('game_time as gameTime')
				  ->from('games_m')
				  ->where('id',$gameId);
				  
			$gameTime = $this->db->get()->row();
			$gameTime = $gameTime->gameTime;
			$dateNow = date("Y-m-d H:i:s");
			if(strtotime($dateNow) - strtotime($gameTime) >= 0){
				
				
				$room = array();
				$isActiveRoom = 1;
			    $room['isActiveRoom'] = 1;
				$room['tag1'] = "Player Name" ;
				$room['tag2'] = "Player Unique ID" ;
				$playerData = $this->common->getPvpOpponentPool($userId,$gameId);
				$room['param1'] = "";
				$room['param2'] = "";
				$isScreenshotNeeded = $this->common->isScreenshotNeeded($gameId,$userId);
				$room['isScreenshotNeeded'] = $isScreenshotNeeded;
				if($playerData){
				$room['param1'] = $playerData->pubgName;
				$room['param2'] = $playerData->uniqueId;
				$room['copy1'] = "0";
				$room['copy2'] = "1";	
				
				}
			}
			
		 }
		 
	 }
	 
	$this->db
    		->select('about as about')
    		->from('game_about')
			->where('game_name',$gameName);
	$result_listabout =  $this->db->get()->result();	
	
	
	if($result_list or $result_listabout){
			 $sampleScreenshot = $this->common->getSampleScreenshot($gameId);
			if((int)$countRows > 0 ){				
				if((int)$isActiveRoom == 1){
					 $packageName = $this->common->getPackagename($gameId);
					 
					return (object)['status' => true, 'data' => $result_list, 'about' => $result_listabout, 'room' => $room,'packageName' => $packageName,'sampleScreenshot' => $sampleScreenshot];
				}else{
					return (object)['status' => true, 'data' => $result_list , 'about' => $result_listabout,'sampleScreenshot' => $sampleScreenshot];
				}
			}else{
    		return (object)['status' => true, 'data' => $result_list, 'about' => $result_listabout,'sampleScreenshot' => $sampleScreenshot];
			}
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no match data found!'];
    	}	 
 }
 
 
 public function getMatch($gameId,$userId){
	 $this->db
    		->select('pubg_name as pubgName')
    		->from('game_participants')
    		->where('game_id', $gameId);
	 $result_list =  $this->db->get()->result();
	$this->db
    		->select('about as about')
    		->from('game_about');
	$result_listabout =  $this->db->get()->result();		
	
	/*$this->db
		 ->select('id as id')
		 ->from('game_participants')
		 ->where('game_id',$gameId)
		 ->where('gamer_id',$userId);*/
	$query = $this->db
				  ->query('SELECT id as Id FROM game_participants where gamer_id = '.$userId.' and  game_id = '.$gameId.' group by game_id');
	 
		 
	$countRows = $query->num_rows();
	
	if((int)$countRows > 0 ){
	$this->db
		 ->select('is_activeroom_credential as isActiveRoom,room_id as roomId,room_pass as roomPass')
		 ->from('games_m')
		 ->where('id',$gameId);
		 
	$room = $this->db->get()->row();
	
	$isActiveRoom = $room->isActiveRoom;
	$roomId = $room->roomId;
	$roomPass = $room->roomPass;
	}
	if($result_list or $result_listabout){
			if((int)$countRows > 0 ){				
				if((int)$isActiveRoom == 1){
					return (object)['status' => true, 'data' => $result_list, 'about' => $result_listabout, 'room' => $room];
				}else{
					
					return (object)['status' => true, 'data' => $result_list, 'about' => $result_listabout];
				}
			}else{
    		return (object)['status' => true, 'data' => $result_list, 'about' => $result_listabout];
			}
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no match data found!'];
    	}	 
 }
 
 
 public function getCoinList ($userId){
	 $this->db
    		->select('coin as coin')
    		->from('coinlist')
			->order_by("id", "asc");
	$result_list =  $this->db->get()->result();		
	 $this->db
    		->select('coins as coins')
    		->from('user')
    		->where('id', $userId);    		
    $data = $this->db->get()->row();
	$coins = $data->coins;
	if($result_list){
    		return (object)['status' => true, 'data' => $result_list, 'coins'=> $coins];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no coin data found!'];
    	}	 
	 
 }
 
 public function getNotification ($userId){
	 $this->db
    		->select('title as title , message as message')
    		->from('game_notification')
			->order_by("id", "desc");
	 $result_list =  $this->db->get()->result();
	 if($result_list){
    		return (object)['status' => true, 'data' => $result_list];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no notification data found!'];
    	}	 
	 
 }
 
 
 private function isApiLocked($gameId){
	 $gameId = $gameId;
	 $this->db
    		 ->select('api_lock as apiLock')
    		 ->from('games_m')
			 ->where("id", $gameId);
	$isApiLocked = $this->db->get()->row();
	$isApiLocked = $isApiLocked->apiLock;
		
	if((int)$isApiLocked == 1){
		sleep(3);	
		$this->isApiLocked($gameId);
	}else{
		$this->setApiLocked($gameId);				
	}
	 
 }
 
 
 private function setApiLocked($gameId){
	 
	 $update_a['api_lock'] = 1;	
	 $this->db  
			 ->where('id', $gameId)			 
    		 ->update('games_m',$update_a);			 
	 $update_resp = $this->db->affected_rows();
	 
 }
 
 private function removeApiLocked($gameId){
	 
	 $update_a['api_lock'] = 0;	
	 $this->db  
			 ->where('id', $gameId)			 
    		 ->update('games_m',$update_a);			 
	 $update_resp = $this->db->affected_rows();
	 
 }
 
 public function addParticipant ($userId,$pubgName,$gameId,$uniqueId){
	 date_default_timezone_set('Asia/Kolkata'); 
	 $referalBonus = 5;
	 $referredUserId = 0; 	 
	$this->db
    		->select('bonus_coins as bonusCoins,coins as coins, referred_user_id as referredUserId,referal_amount_disbursed as referalAmountDisbursed,is_banned as isBanned')
    		->from('user')
			->where("id", $userId);
	$userData =  $this->db->get()->row();
	
	$isBanned = $userData->isBanned;
	if($isBanned){
		return (object)['status' => false, 'message' => 'YOU ARE BANNED FOR HACKING IN GAME! Contact cratehunt.2019@gmail.com for further proceedings'];		
	}
	
	$coins = $userData->coins ;
	$bonusCoins = $userData->bonusCoins ;
	$referredUserId = $userData->referredUserId;
	$referalAmountDisbursed =  $userData->referalAmountDisbursed;
	$this->db
    		->select('game_name as gameName,entry_fee as entryFee , total_gamers as totalGamers')
    		->from('games_m')
			->where("id", $gameId);
	$gameData =  $this->db->get()->row();
	$gameFee = $gameData->entryFee;
	$gameName = $gameData->gameName;
	$bonusCoinInt = (int)$bonusCoins;
	$coinInt = (int)$coins; 
	$feeInt = (int)$gameFee;
	$totalGamers = (int)$gameData->totalGamers;
	
	$totalCoin = $bonusCoinInt + $coinInt;
	
	
	if($totalCoin >= $feeInt){
		
		
	$this->db
    		->select('COUNT(id) as count')
    		->from('game_participants')
			->where("game_id", $gameId)
			->where("gamer_id", $userId);

	$countParicipant = $this->db->get()->row();		
	$countParicipant = 	$countParicipant->count;
	if($countParicipant > 0 and $feeInt == 0){
		 return (object)['status' => false, 'message' => 'All participants should register individually. Only one seat in free match'];		
	}	
		
	 $this->db
    		->select('COUNT(id) as count')
    		->from('game_participants')
			->where("game_id", $gameId)
			->where("pubg_name", $pubgName);
			
	 		
	 $count = $this->db->get()->row();
	 $count = $count->count;
	 if((int)$count > 0){
		 return (object)['status' => false, 'message' => 'Sorry, This player already exist in the match'];
		 
	 }

	
	 
	 $isRoomFull = $this->common->isRoomFull($gameId);
	 if($isRoomFull){
		  return (object)['status' => false, 'message' => 'Sorry, The room is full join another room'];		 
	 }
	 
	 
		
		if($totalGamers == 2){
			$this->db
				->select('COUNT(id) as count')
				->from('game_participants')
				->where("game_id", $gameId)
				->where("gamer_id", $userId);
				
			$count = $this->db->get()->row();
			$count = $count->count;
			 if((int)$count > 0){
				 return (object)['status' => false, 'message' => 'Sorry, Only one seat per game in PVP mode'];
			 }
		}
		
		$this->isApiLocked($gameId);
		$isRoomFull = $this->common->isRoomFull($gameId);
		if($isRoomFull){
		  return (object)['status' => false, 'message' => 'Sorry, The room is full join another room'];		 
		}
		
		
		$allotedCoins = 0;
		$allotedBonus = 0;
		$newBonusCoinInt = 0;
		$newCoins = 0;
		if($bonusCoinInt >= $feeInt){
			$newBonusCoinInt = $bonusCoinInt - $feeInt;
			$newCoins = $coinInt;
			$allotedBonus = $feeInt;
		}elseif($bonusCoinInt == 0){
			$newBonusCoinInt = 0;
			$newCoins = $coinInt - $feeInt ;
			$allotedCoins = $feeInt;
		}elseif($bonusCoinInt < $feeInt and $bonusCoinInt >0){			
			$newBonusCoinInt = 0;
			//$newCoins = $coinInt - ($feeInt - $bonusCoinInt);
			$newCoins = $coinInt - $feeInt + $bonusCoinInt;						
			$allotedCoins = $feeInt - $bonusCoinInt;
			$allotedBonus = $bonusCoinInt;
		}
		

	
			

		$update_a = array();
		$update_a['coins'] = $newCoins;
		$update_a['bonus_coins'] = $newBonusCoinInt;
		
		$this->db  
			 ->where('id', $userId)			 
    		 ->update('user',$update_a);			 
		$update_resp = $this->db->affected_rows();
		
    	
			$timelineData = array();
			$timelineData['userId'] = $userId;
			$timelineData['type'] = "dr";
			$timelineData['detail'] = "joined a match with game Id ".$gameId;
			$timelineData['amt'] = $feeInt;
			$this->common->addToTimeline($timelineData);
			$isnert_a  = array();
			if($uniqueId != "0"){
				$isnert_a = ['game_id' => $gameId, 'gamer_id' => $userId, 'pubg_name' => $pubgName,'player_unique_id' => $uniqueId, 'coins' => $allotedCoins,'bonus_coins' => $allotedBonus];
			}else{
			
				$isnert_a = ['game_id' => $gameId, 'gamer_id' => $userId, 'pubg_name' => $pubgName, 'coins' => $allotedCoins,'bonus_coins' => $allotedBonus];
			}
			$this->db->insert('game_participants', $isnert_a);
			$inserted_id = $this->db->insert_id();
			if($inserted_id){
				$this->removeApiLocked($gameId);
				if($feeInt != 0 and $referredUserId > 0){ // for checking if match is paid or not and is he signed up with referal code
					
					if((int)$referalAmountDisbursed == 0){
						$this->db
							 ->select('bonus_coins as bonusCoins')
    		                 ->from('user')
			                 ->where("id", $referredUserId);
						$referredUserCoins = $this->db->get()->row();	 
						$referredUserCoins = $referredUserCoins->bonusCoins;
						$referalBonus = (int)$referalBonus + (int)$referredUserCoins;
						$this->db
						->where('id', $referredUserId)
						->set('bonus_coins',$referalBonus)
						->update('user');
						$update_res = $this->db->affected_rows();
						if($update_res){
							$this->db
								 ->where('id', $userId)
								 ->set('referal_amount_disbursed', '1')
								 ->update('user');
							$update_res = $this->db->affected_rows();
							if($update_res){
								$timelineData = array();
								$timelineData['userId'] = $referredUserId;
								$timelineData['type'] = "cr";
								$timelineData['detail'] = "Referral Bonus";
								$timelineData['amt'] = $referalBonus;
								$this->common->addToTimeline($timelineData);
								
								return (object)['status' => true, 'data' => 'Gamer Added to Match Successfully and your referal got his bonus coins!'];
							}
						}
					}else{
						
				return (object)['status' => true, 'data' => 'Gamer Added to Match Successfully'];
				}
				}else{
					
				return (object)['status' => true, 'data' => 'Gamer Added to Match Successfully'];
				}
				
			}else{
				return (object)['status' => false, 'message' => 'Sorry, please try again later2'];
			}
		
	  
	}else{
		return (object)['status' => false, 'message' => 'insufficient coins in wallet'];
	}
 }
 
 
 
  public function getBenificiaryStatus($userId){
	  $this->db
    		->select('ben_id as benId')
    		->from('user')
			->where("id", $userId);
			
	 $data = $this->db->get()->row();
	 $benId = $data->benId;
	 if($benId){
		 if(strlen($benId) > 0 ){			 
			return (object)['status' => true, 'data' => 'benificiary active','benId' => $benId];		 
		 }else{			 
			 return (object)['status' => false, 'message' => 'benificiary inactive'];
		 }
	 }
	 else{
		 return (object)['status' => false, 'message' => 'benificiary inactive'];
	 }
	  
  }
  
  
 public function updateParticipationStatus($userId,$gameId){
	  
	  $this->db
			->where('gamer_id', $userId)
			->where('game_id', $gameId)
			->set('is_participated', '1')
			->update('game_participants');
			
	   return (object)['status' => true, 'data' => 'update successfull'];	
  }
  
  
 
 public function removeAllParticipants($userId,$gameId){
	 
	  $this->db
    		->select('COUNT(id) as count')
    		->from('game_participants')
			->where("game_id", $gameId)
			->where("gamer_id", $userId);
			
	 $count = $this->db->get()->row();
	 $count = $count->count;
	 if((int)$count > 0){
		 
		 $this->db
			  ->where('game_id', $gameId)
			  ->where('gamer_id', $userId);

		$delete = $this->db->delete('game_participants');	  
			  
			  
		$this->db
    		->select('coins as coins')
    		->from('user')
			->where("id", $userId);	  
		$coins = $this->db->get()->row();
		$coins = $coins->coins;	  
		
		
		
		
		$this->db
    		->select('entry_fee as entryFee')
    		->from('games_m')
			->where("id", $gameId);
		$entryFee = $this->db->get()->row();
		$entryFee = $entryFee->entryFee;	
		
		
		$refundedCoins = (int)$entryFee * (int)$count;
		
		$newBalance = $refundedCoins + (int)$coins;

		$this->db
    		 ->where('id', $userId)
    		 ->set('coins', $newBalance)
    		 ->update('user');
		$update_res = $this->db->affected_rows();
		
    	if($update_res){
			
			$timelineData = array();
			$timelineData['userId'] = $userId;
			$timelineData['type'] = "cr";
			$timelineData['detail'] = "Refund on unjoin a match with game Id ".$gameId;
			$timelineData['amt'] = $refundedCoins;
			$this->common->addToTimeline($timelineData);
			
			return (object)['status' => true, 'data' => 'You have successfully unjoined this match'];
		}else{
			
			 return (object)['status' => false, 'message' => 'Sorry something went wrong!'];
		}
		
		
		
		
		 
		 
	 }else{
		 
		 return (object)['status' => false, 'message' => 'Sorry, You have not joined this match'];
		 
	 }
			
	 
 }
 
}
 

