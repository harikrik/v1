<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes with
| underscores in the controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;



$route['api/signup']     = 'api/Login/signup';
$route['api/varify-otp']     = 'api/Login/varifyOtp';
$route['api/update-profile']     = 'api/Login/updateProfile';
$route['api/get-profile']     = 'api/Login/getProfileData';
$route['api/live-games']     = 'api/Game/getLiveGames';
$route['api/live-games-byname']     = 'api/Game/getLiveGamesByName';
$route['api/ongoing-games']     = 'api/Game/getOngoingGames';
$route['api/user-coins']     = 'api/Game/getUserCoins';
$route['api/match']     = 'api/Game/getMatch';
$route['api/match-result']     = 'api/Game/getGameResult';
$route['api/match-result-byname']     = 'api/Game/getGameResultByName';
$route['api/match-detail']     = 'api/Game/getGameResultDetail';
$route['api/notification']     = 'api/Game/getNotification';
$route['api/add-participants']     = 'api/Game/addParticipant';
$route['api/remove-allparticipants']     = 'api/Game/removeAllParticipants';
$route['api/update-participation-status']     = 'api/Game/updateParticipationStatus';


$route['api/ben-status'] = 'api/Game/getBenificiaryStatus';

$route['api/coin-list']     = 'api/Game/getCoinList';
$route['api/withdraw-fund']     = 'api/Game/withdrawFund';
$route['api/file-upload']     = 'api/Game/uploadFile';
$route['api/resendOtp']     = 'api/Login/resendOtp';
$route['api/getReferalCode']     = 'api/Login/getReferalCode';
$route['api/aboutUs']     = 'api/Login/aboutUs';
$route['api/update-pubgName']     = 'api/Login/updatePubgName';


//$route['api/create-order']     = 'api/Payment/createOrder';
$route['api/payment-status']     = 'api/Payment/paymentStatus';
$route['api/create-order']     = 'api/Payments/payby_paytm';
$route['api/paytm-resp']     = 'api/Payments/paytm_response';
$route['api/add-firebasetoken']     = 'api/Login/addUserFirebaseDetails';
$route['api/remove-firebasetoken']     = 'api/Login/deleteUserFirebaseDetails';
$route['api/send-notification']     = 'api/Login/sendNotification';
$route['api/send-notification-bygame']     = 'api/Login/sendNotificationSpecificAudience';
$route['api/app-state']     = 'api/Login/getAppState';

$route['api/test-encrypt']     = 'api/Login/testEncrypt';
$route['api/test-decrypt']     = 'api/Login/testDecrypt';

$route['api/encrypt']     = 'api/Game/encrypt';


$route['api/admin-login']     = 'api/Admin/login';
$route['api/admin-live-games']     = 'api/Admin/getLiveGames';
$route['api/admin-create-match']     = 'api/Admin/createMatch';
$route['api/admin-live-game-detail']     = 'api/Admin/getLiveGamesDetail';
$route['api/admin-update-match-result']     = 'api/Admin/updateMatchResult';
$route['api/admin-cancel-match']     = 'api/Admin/cancelMatch';


$route['api/add-benificiary']     = 'api/Payout/addBenificiery';
$route['api/download/(:any)']     = 'api/Download/download';


