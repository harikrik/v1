<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

	protected $CI;

	public function __construct()
    {
        $this->CI = & get_instance();
		
    }
	

	public function jsonOutput($data = array() ){
		$this->CI->output->set_content_type('application/json');
		echo json_encode($data);
		exit;
	}
	
	public function otpGenerator($digits = 4){
        $pow_num = pow(10, $digits)-1;
        $token = str_pad(rand(0, intval($pow_num)), $digits, '0', STR_PAD_LEFT);
		
        return $token;
	}
	
	
	public function getPvpOpponent($userId,$gameId){
		$this->db = $this->CI->load->database('default',TRUE);
		$this->db
			 ->select('pubg_name as pubgName')
			 ->from('game_participants')
			 ->where('game_id', $gameId)
			 ->where('gamer_id != ', $userId);
		$data = $this->db->get()->row();
		if($data){
		$playerName = $data->pubgName;
		return $playerName;
		}else{
		return false;	 
		}
		
	}
	
	public function getPackagename($gameId){
		$this->db = $this->CI->load->database('default',TRUE);		
		$packageName = "";
		$this->db
			 ->select('gl.package_name as packageName')
			 ->from('games_m as gm')
			 ->join('game_list as gl','gm.game_name = gl.game_name')			 
			 ->where('gm.id', $gameId);
		$data = $this->db->get()->row();
		if($data){
			$packageName = $data->packageName;
		}		
		return $packageName;
	}
	
	public function getSampleScreenshot($gameId){
		$this->db = $this->CI->load->database('default',TRUE);		
		$sampleScreenshot = "";
		$this->db
			 ->select('gl.sample_screenshot as sampleScreenshot')
			 ->from('games_m as gm')
			 ->join('game_list as gl','gm.game_name = gl.game_name')			 
			 ->where('gm.id', $gameId);
		$data = $this->db->get()->row();
		if($data){
			$sampleScreenshot = $data->sampleScreenshot;
		}		
		return $sampleScreenshot;
	}
	
	
	public function getPvpOpponentPool($userId,$gameId){
		$this->db = $this->CI->load->database('default',TRUE);
		$this->db
			 ->select('pubg_name as pubgName,player_unique_id as uniqueId')
			 ->from('game_participants')
			 ->where('game_id', $gameId)
			 ->where('gamer_id != ', $userId);
			 
		$data = $this->db->get()->row();	
		if($data){
		return $data;
		}else{
			return false;
		}	 
		
	}
	
	
	
	public function referalCodeGenerator($length = 6) {
		$this->db = $this->CI->load->database('default',TRUE);
		$alphabets = range('A','Z');
		$numbers = range('0','9');
	    //$additional_characters = array('_','.');
		//$final_array = array_merge($alphabets,$numbers,$additional_characters);
		$final_array = array_merge($alphabets,$numbers);
			 
		$password = '';
	  
		while($length--) {
		  $key = array_rand($final_array);
		  $password .= $final_array[$key];
		}
		$data = 1;
		$this->db
				->select('id as id')
				->from('user')
				->where('referral_code', $password);
		$data = $this->db->get()->row();		
		if($data){		
			$this->referalCodeGenerator();
		}  
		return $password;
  }
  
  
  public function base64ToImage($base64_string, $output_file) {
    $file = fopen($output_file, "wb");
    //$data = explode(',', $base64_string);
    //fwrite($file, base64_decode($data[1]));
    fwrite($file, base64_decode($base64_string));
    fclose($file);
    return $output_file;
}
  
	public function getReferalUserId($referalCode){
		$this->db = $this->CI->load->database('default',TRUE);
		$this->db
				->select('id as id')
				->from('user')
				->where('referral_code', $referalCode);
		$data = $this->db->get()->row();
		$refUserId = $data->id;
		return $refUserId;
	}
	
	public function addToTimeline($data){
		date_default_timezone_set('Asia/Kolkata'); 
		$this->db = $this->CI->load->database('default',TRUE);
		$txnUserId = $data['userId'];
		$txnType = $data['type'];
		$txnDetail = $data['detail'];
		$txnAmt = $data['amt'];
		$txnDate = date("Y-m-d H:i:s");		
		$isnert_a = ['user_id' => $txnUserId, 'txn_type' => $txnType, 'detail' => $txnDetail, 'coins' => $txnAmt, 'datetime' => $txnDate];
		$this->db->insert('timeline', $isnert_a);
		$inserted_id = $this->db->insert_id();
		if($inserted_id){
			return (object)['status' => true, 'data' => 'Added to timeline Successfully'];
			
		}else{
			return (object)['status' => false, 'message' => 'Sorry, please try again later'];
			
		}
		
		
	}
	
	
	public function updateUserCoin($gameId,$gamerId,$amount){
		date_default_timezone_set('Asia/Kolkata'); 
		$this->db = $this->CI->load->database('default',TRUE);
		$this->db
			->select('coins as coins')
			->from('user')
			->where('id', $gamerId);
		$data = $this->db->get()->row();
		$coins = $data->coins;				 
		$newBalance = (int)$coins + (int)$amount;
		$this->db
    		 ->where('id', $gamerId)
    		 ->set('coins', $newBalance)
    		 ->update('user');
			 
		$update_res = $this->db->affected_rows();
		
    	if($update_res){ // if there is any change in user coins then only update
			$txnDate = date("Y-m-d H:i:s");	
			$txnDetail = "Earned from match with game Id ".$gameId;
			$isnert_a = ['user_id' => $gamerId, 'txn_type' => 'cr', 'detail' => $txnDetail, 'coins' => $amount, 'datetime' => $txnDate];
			$this->db->insert('timeline', $isnert_a);
			$inserted_id = $this->db->insert_id();	
		}
		
		return true;
		
	}
	
	public function isScreenshotNeeded($gameId,$userId){
		$this->db = $this->CI->load->database('default',TRUE);
		$this->db
			->select('screenshot as screenshot')
			->from('game_participants')
			->where('game_id',$gameId)
			->where('gamer_id',$userId);
		$data = $this->db->get()->row();
		$screenshot = $data->screenshot;
		if($screenshot){
			return "0";
		}else{
			return "1";
		}
	}
	
	
	public function isRoomFull($gameId){
		$this->db = $this->CI->load->database('default',TRUE);
		$this->db
			->select('gm.total_gamers as totalGamers,count(gp.id) as count')
			->from('game_participants as gp')
			->join('games_m as gm','gm.id = gp.game_id')
			->where('game_id', $gameId);
		$data = $this->db->get()->row();
		$totalGamers = $data->totalGamers;
		$JoinedGamers = $data->count;	
		if($JoinedGamers < $totalGamers){
			return false;
		}else{
			return true;
		}
		
	}
	
	
	public function initiateRefund($gameId,$gamerId){
		date_default_timezone_set('Asia/Kolkata'); 
		$this->db = $this->CI->load->database('default',TRUE);
		$this->db
			->select('coins as coins, bonus_coins as bonusCoins')
			->from('game_participants')
			->where('game_id', $gameId)
			->where('gamer_id', $gamerId);
		$data = $this->db->get()->row();
		$usedCoins = $data->coins;
		$usedBonus = $data->bonusCoins;
		$this->db
			->select('coins as coins,bonus_coins as bonusCoins')
			->from('user')
			->where('id', $gamerId);
		$data = $this->db->get()->row();
		$coins = $data->coins;				 
		$bonusCoins = $data->bonusCoins;	
		
		$newCoins = (int)$coins + (int)$usedCoins;
		$newBonus = (int)$bonusCoins + (int)$usedBonus;
		
		$this->db
    		 ->where('id', $gamerId)
    		 ->set('coins', $newCoins)
    		 ->set('bonus_coins', $newBonus)
    		 ->update('user');
			 
		$update_res = $this->db->affected_rows();
		
    	if($update_res){ // if there is any change in user coins then only update
			$txnDate = date("Y-m-d H:i:s");	
			$txnDetail = "refund from match with game Id ".$gameId;
			if($usedBonus > 0){
				$isnert_a = ['user_id' => $gamerId, 'txn_type' => 'cr', 'detail' => $txnDetail, 'coins' => $usedCoins, 'bonus_coins' => $usedBonus, 'datetime' => $txnDate];
			}else{
				$isnert_a = ['user_id' => $gamerId, 'txn_type' => 'cr', 'detail' => $txnDetail, 'coins' => $usedCoins, 'datetime' => $txnDate];
			}
			
			$this->db->insert('timeline', $isnert_a);
			$inserted_id = $this->db->insert_id();	
		}
		
		
		
	}
	
	
	public function updateUserBonusCoin($gameId,$gamerId,$amount){
		
		date_default_timezone_set('Asia/Kolkata'); 
		$this->db = $this->CI->load->database('default',TRUE);
		$this->db
			->select('bonus_coins as coins')
			->from('user')
			->where('id', $gamerId);
		$data = $this->db->get()->row();
		$coins = $data->coins;				 
		$newBalance = (int)$coins + (int)$amount;
		$this->db
    		 ->where('id', $gamerId)
    		 ->set('bonus_coins', $newBalance)
    		 ->update('user');
			 
		$update_res = $this->db->affected_rows();
		
    	if($update_res){ // if there is any change in user coins then only update
			$txnDate = date("Y-m-d H:i:s");	
			$txnDetail = "refund from match with game Id ".$gameId;
			$isnert_a = ['user_id' => $gamerId, 'txn_type' => 'cr', 'detail' => $txnDetail, 'bonus_coins' => $amount, 'datetime' => $txnDate];
			
			$this->db->insert('timeline', $isnert_a);
			$inserted_id = $this->db->insert_id();	
		}
		
		return true;
	}
	
	

	
	
	public function payoutTokenGenerator(){
		$this->db = $this->CI->load->database('default',TRUE);
		
		//"X-Client-Id: CF8626GPHQ9AH8JXUAUUQ",
		//	"X-Client-Secret: 706d8b0fccda0e08a8fe4562fb85998fd9a50470"
       $curl = curl_init();
        $url = "https://payout-api.cashfree.com/payout/v1/authorize";
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_HTTPHEADER => array(			
			"X-Client-Id: CF27946CMYXI3DHFT8IEMU",
			"X-Client-Secret: 3ad3d55a96162ff2492270f86acdf182915f989c"
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		  return "error";
		} else {
		   $response = json_decode($response, true);
		   $response = $response['data']['token'];		   
		   
		   
		  
		 //sleep(25);  
		 return $response;
		   }
	}
	
	public function unsetValue(array $array, $value, $strict = true)
{
    if(($key = array_search($value, $array, $strict)) !== false) {
        unset($array[$key]);
    }
    return $array;
}

public function unsetNew($data) {

    if (is_object($data)) {
        $data = get_object_vars($data);
    }

    if (is_array($data)) {
        return array_map(__FUNCTION__, $data);
    }
    else {
        return $data;
    }
}

	
	public function addPayoutBenificery($token,$user){
		
        $curl = curl_init();
        $url = "https://payout-api.cashfree.com/payout/v1/addBeneficiary";
		$data = array("beneId" => $user["beneId"], "name" => $user["name"],"email" => $user["email"], "phone" => $user["phone"],
		"bankAccount" => $user["bankAccount"], "ifsc" => $user["ifsc"], "address1" => "NA");  		
		$data_string = json_encode($data); 
		//echo($data_string);
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $data_string,
		  CURLOPT_HTTPHEADER => array(	
		  "Authorization: Bearer ".$token,
			"accept: application/json",
			"content-type: application/json"
			),
		));		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		  return $err;
		} else {
		  // echo($response);
		   return $response;
		}			
	}	
	
	
	
	

	public function validateToken($token){
		
        $curl = curl_init();
        $url = "https://payout-api.cashfree.com/payout/v1/verifyToken";		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_HTTPHEADER => array(			
			"Authorization: Bearer ".$token,
			"content-type: application/json"
			),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		  return (object)['status' => false, 'data' => $err];
		} else {
			
		   return (object)['status' => true, 'data' => $response];
		}		
		
		
	}
	
	
	public function validateBankDetails($token,$mob,$accountHolderName,$accountNumber,$ifsc){
		
        $curl = curl_init();
		//$query = "name=".urlencode($accountHolderName)."&phone=".$mob."&bankAccount=".$accountNumber."&ifsc=".$ifsc;
		$query = "name=".urlencode($accountHolderName)."&phone=".$mob."&bankAccount=000100289877623&ifsc=SBIN0008752";
        $url = "https://payout-api.cashfree.com/payout/v1/validation/bankDetails?".$query;		
		$header = array(			
			"Authorization: Bearer ".$token,
			"content-type: application/json"
			);
			
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => $header,
		));		
		$response = curl_exec($curl);		
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {			
		  return (object)['status' => false, 'data' => $err];
		} else {
		   return (object)['status' => true, 'data' => $response];
		}		
		
		
	}
	
	
	
	
	
	public function requestTransfer($token,$user){
		
        $curl = curl_init();

        $url = "https://payout-api.cashfree.com/payout/v1/requestTransfer";		
		
		
		$data = array("beneId" => $user["beneId"], "amount" => $user["amount"],"transferId" => $user["transferId"],"transferMode" => $user["transferMode"]);  		
		$data_string = json_encode($data); 
		//echo($data_string);
	
		$header = array(			
			"Authorization: Bearer ".$token,
			"content-type: application/json"
			);
			
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $data_string,
		  CURLOPT_HTTPHEADER => $header,
		));		
		$response = curl_exec($curl);
		
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		  return (object)['status' => false, 'data' => $err];
		} else {
		   return (object)['status' => true, 'data' => $response];
		}		
		
		
	}
	
	public function decryptData($crypto,$config,$encryptedString){
		$data = $this->decrypt($crypto,$config,$encryptedString);
		if($data){
				return $data;
		}
	}
	
	public function encryptData($crypto,$config,$plainText){
		//$data = $this->encrypt($crypto,$config,$plainText);
		$data = $this->encryptNew($crypto,$config,$plainText);
		
		return $data;
		
	}
	
	public function encrypt($crypto,$config,$plainText){
		$key = hex2bin($config->item('key'));	 
		$crypto->initialize(
				array(
						'cipher' => 'aes-256',
						'mode' => 'cbc',
						'driver' => 'openssl',
						'key' => $key
				)	
			);
		$ciphertext = $crypto->encrypt($plainText);
		return $ciphertext;
		
	}
	
	public function encryptNew($crypto,$config,$plainText){	
			$key = hex2bin($config->item('key'));
			$plaintext = $plainText;
			$ivlen = openssl_cipher_iv_length($cipher="AES-256-CBC");			
			$iv = openssl_random_pseudo_bytes($ivlen);
			$ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
			$hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
			$ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
			//$ciphertext = base64_encode( $iv.$ciphertext_raw );
			$data = array();
			$data['data'] = $ciphertext;
			$data['iv'] = base64_encode($iv);			
			return $data;

	}
	
	public function decrypt($crypto,$config,$encryptedString){
		$key = hex2bin($config->item('key'));	 
		$crypto->initialize(
				array(
						'cipher' => 'aes-256',
						'mode' => 'cbc',
						'driver' => 'openssl',
						'key' => $key
				)	
			);
		$plaintext = $crypto->decrypt($encryptedString);
		return $plaintext;
		
	}
	
	public function encryptCrossplatform($crypto,$config,$plainText){
			
			$data = array();
			//$secretKey = hex2bin($config->item('key'));
			$secretKey = $config->item('key');
			//var_dump($secretKey);
			$text = $plainText;	
			
			$encrypted = $crypto::encrypt($secretKey,$text);
			
			if(!$encrypted->hasError()){
				//echo("0");
				$ciphertext = $encrypted->getData(); 
				//$iv = $encrypted->getInitVector();				
				$data['status'] = 1;
				$data['data'] = $ciphertext;
				//$data['iv'] = $iv;				
			}else{			
				//echo("1");
				$data['status'] = 0;				
			} 
			
			return $data;
		
	}
	
	public function decryptCrossplatform($crypto,$config,$encrypted){
			$data = array();
			
			//$secretKey = hex2bin($config->item('key'));	
			
			$secretKey = $config->item('key');						
			$decrypted = $crypto::decrypt($secretKey, $encrypted);
			
			if(!$decrypted->hasError()){
				
				$text = $decrypted->getData();				
				$data['status'] = 1;
				$data['data'] = $text;
			}else{				
				
				$data['status'] = 0;
			}
			 return $data;
		
	}	
	

	
}

?>