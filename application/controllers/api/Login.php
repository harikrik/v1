<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	private $result = '';
	private $validation = '';
	private $insert_a = [];

	public function __construct(){
        parent::__construct();
		//$this->load->library('encryption');
		$params[] = null;
		$this->load->library('aescipher',$params);
		$this->config->load('config');
		$this->load->model('Login_model');
		
    }
	
	

	
	
	public function aboutUs(){
		
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);				 
        }
        $this->result = $this->Login_model->aboutUs($userId);
		$data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);       
		
		
	}
	
	
	
	

    /**
     * signup function created with mobile number. after signup api return an otp 
     * @return [type] [description]
     */
	public function signup(){
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);		
		$mobileNumber = isset($json_obj->mobileNumber) ? trim($json_obj->mobileNumber): '';
		
		if( !$mobileNumber ){			  
			$message = ['status'=> 'fail', 'message' => 'mobileNumber is blank'];
			$data = $this->encrypt($crypto,$config,$message);
			$this->jsonOutput(['query' =>  $data]);			
        }
		$this->validation = $this->validateMobileNumber($mobileNumber);		
        if(!$this->validation){        	
			$message = ['status'=> 'fail', 'message' => 'Please enter a valid mobile number'];
			$data = $this->encrypt($crypto,$config,$message);
			$this->jsonOutput(['query' =>  $data]);
        }    

        if( array_key_exists( 'error', $this->validation ) ){
        	if($this->validation['error'] == 'invalid mobile'){
				$message = ['status' => false, 'message' => $this->validation['message']];
        		$data = $this->encrypt($crypto,$config,$message);
				$this->jsonOutput(['query' =>  $data]);
        	}else{        	
				$message = ['status' => true, 'message' => 'otp generated'];
        		$data = $this->encrypt($crypto,$config,$message);
				$this->jsonOutput(['query' =>  $data]);
        	}
        }

        $otp = $this->common->otpGenerator(4);
        $this->result = $this->Login_model->signup($mobileNumber, $otp);
        $data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
		
	}

	/**
	 * To varify the mobile and otp, and activating the account
	 * @return [type] [description]
	 */
	public function varifyOtp(){
		$config = $this->config;			
		$crypto = $this->aescipher;
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);
		$json_obj = json_decode($json_obj);	
		$otp = isset($json_obj->otp) ? trim($json_obj->otp): '';
		$mobileNumber = isset($json_obj->mobileNumber) ? trim($json_obj->mobileNumber): '';

		if( !$otp ){			
			$message = ['status'=> 'fail', 'message' => 'otp is blank'];
        	$data = $this->encrypt($crypto,$config,$message);
			$this->jsonOutput(['query' =>  $data]);
        }
		if( !$mobileNumber ){
			$message = ['status'=> 'fail', 'message' => 'mobileNumber is blank'];
        	$data = $this->encrypt($crypto,$config,$message);
			$this->jsonOutput(['query' =>  $data]);			    
        }

        $this->validation = $this->Login_model->validateOtp($otp, $mobileNumber);
        if($this->validation->status === true){        	
        	$data = $this->encrypt($crypto,$config,$this->validation);
			$this->jsonOutput(['query' =>  $data]);        	
        }else{
        	$data = $this->encrypt($crypto,$config,$this->validation);
			$this->jsonOutput(['query' =>  $data]);
        }
	}
	
	
	public function resendOtp(){
		$config = $this->config;			
		$crypto = $this->aescipher;
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);
		$json_obj = json_decode($json_obj);	
		$mobile = isset($json_obj->mobile) ? trim($json_obj->mobile): '';
		if( !$mobile ){
			$message = ['status'=> 'fail', 'message' => 'mobile is blank'];
        	$data = $this->encrypt($crypto,$config,$message);
			$this->jsonOutput(['query' =>  $data]);
			
        }

        $this->result = $this->Login_model->resendOtp($mobile);
		$data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);        
		
	}
	
	public function updatePubgName(){
		$config = $this->config;			
		$crypto = $this->aescipher;
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);
		$json_obj = json_decode($json_obj);
		$playerData = $json_obj->playerData;		
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
        	$data = $this->encrypt($crypto,$config,$message);
			$this->jsonOutput(['query' =>  $data]);			   
        }
		$this->result = $this->Login_model->updatePubgName($userId,$playerData);		
		$data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
	}

	/**
	 * To update profile and account details
	 * @return [type] [description]
	 */
	public function updateProfile(){
		$config = $this->config;			
		$crypto = $this->aescipher;
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);
		$json_obj = json_decode($json_obj);
		$referalUserId = null;
		$mobileNumber = isset($json_obj->mobileNumber) ? trim($json_obj->mobileNumber): '';
		$firstName = isset($json_obj->firstName) ? trim($json_obj->firstName): '';		
		$email = isset($json_obj->email) ? trim($json_obj->email): '';
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';
		$referalCode = isset($json_obj->referalCode) ? trim($json_obj->referalCode): '';
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
        	$data = $this->encrypt($crypto,$config,$message);
			$this->jsonOutput(['query' =>  $data]);
			  
        }

		if($email){
			$this->validEmail($email);
			$this->insert_a['email'] = $email;
		}
		if($mobileNumber){
			$this->validation = $this->validateMobileNumber($mobileNumber);
			if($this->validation['error'] == 'invalid mobile'){
				$message = ['status' => false, 'message' => $this->validation['message']];
				$data = $this->encrypt($crypto,$config,$message);
				$this->jsonOutput(['query' =>  $data]);
				
			}
			$this->insert_a['mobile'] = $mobileNumber;
		}
		if($firstName){
			$this->insert_a['first_name'] = $firstName;
		}		
		if($referalCode){
			$referalUserId = $this->common->getReferalUserId($referalCode);			
		}
		
		$myReferalCode = $this->common->referalCodeGenerator();
		$this->insert_a['referral_code'] = $myReferalCode;
		$this->insert_a['referred_user_id'] = $referalUserId;
		$this->insert_a['bonus_coins'] = 5;
		$this->result = $this->Login_model->updateProfile($this->insert_a, $userId);		
		$this->result_new = $this->Login_model->activateAccount($userId);
		$data = $this->encrypt($crypto,$config,$this->result_new);						
		$this->jsonOutput(['query' =>  $data]);
	}

	/**
	 * return user profile data with respect to the userId given
	 * @return [type] [description]
	 */
	public function getProfileData(){
		$config = $this->config;			
		$crypto = $this->aescipher;
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);
		$json_obj = json_decode($json_obj);
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';

		if( !$userId ){
				$message = ['status'=> 'fail', 'message' => 'userId is blank'];
				$data = $this->encrypt($crypto,$config,$message);
				$this->jsonOutput(['query' =>  $data]);
				   
        }

        $this->result = $this->Login_model->getProfile($userId);
		$data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
	}
	
	
	public function getReferalCode(){
		$config = $this->config;			
		$crypto = $this->aescipher;
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);
		$json_obj = json_decode($json_obj);
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';
		if( !$userId ){
				$message = ['status'=> 'fail', 'message' => 'userId is blank'];
				$data = $this->encrypt($crypto,$config,$message);
				$this->jsonOutput(['query' =>  $data]);
			
        }
        $this->result = $this->Login_model->getReferalCode($userId);
		$data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
		
	}
	
	
	public function addUserFirebaseDetails(){
		
		$config = $this->config;			
		$crypto = $this->aescipher;
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);
		$json_obj = json_decode($json_obj);
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';
		$token = isset($json_obj->token) ? trim($json_obj->token): '';
		$deviceId = isset($json_obj->deviceId) ? trim($json_obj->deviceId): '';
		$deviceType = isset($json_obj->deviceType) ? trim($json_obj->deviceType): '';
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
			$data = $this->encrypt($crypto,$config,$message);
			$this->jsonOutput(['query' =>  $data]);
			   
        }
		if( !$token ){
			$message = ['status'=> 'fail', 'message' => 'token is blank'];
			$data = $this->encrypt($crypto,$config,$message);
			$this->jsonOutput(['query' =>  $data]);			 
        }
		if( !$deviceId ){
			$message = ['status'=> 'fail', 'message' => 'deviceId is blank'];
			$data = $this->encrypt($crypto,$config,$message);
			$this->jsonOutput(['query' =>  $data]);
			 
        }
		if( !$deviceType ){
			$message = ['status'=> 'fail', 'message' => 'deviceType is blank'];
			$data = $this->encrypt($crypto,$config,$message);
			$this->jsonOutput(['query' =>  $data]);			  
        }
        $this->result = $this->Login_model->addUserFirebaseDetails($userId,$token,$deviceId,$deviceType);
        $data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
		
	}
	
	public function deleteUserFirebaseDetails(){
		$config = $this->config;			
		$crypto = $this->aescipher;
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		$deviceId = isset($json_obj->deviceId) ? trim($json_obj->deviceId): '';		
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
			$data = $this->encrypt($crypto,$config,$message);
			$this->jsonOutput(['query' =>  $data]);    
        }
		if( !$deviceId ){
			$message = ['status'=> 'fail', 'message' => 'deviceId is blank'];
			$data = $this->encrypt($crypto,$config,$message);
			$this->jsonOutput(['query' =>  $data]);    
        }
		$this->result = $this->Login_model->deleteUserFirebaseDetails($userId,$deviceId);
		$data = $this->encrypt($crypto,$config,$this->result);
        $this->jsonOutput(['query' =>  $data]);
		
	}
	
	public function sendNotification(){		
		$json_obj = $this->readJson();
		$title = isset($json_obj->title) ? trim($json_obj->title): '';		
		$message = isset($json_obj->message) ? trim($json_obj->message): '';		
		if( !$title ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'title is blank']);    
        }
		if( !$message ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'message is blank']);    
        }
		$this->result = $this->Login_model->sendNotification($title,$message);
        $this->jsonOutput($this->result);
		
	}
	
	public function sendNotificationSpecificAudience(){		
		$json_obj = $this->readJson();
		$title = isset($json_obj->title) ? trim($json_obj->title): '';		
		$message = isset($json_obj->message) ? trim($json_obj->message): '';		
		$gameId = isset($json_obj->gameId) ? trim($json_obj->gameId): '';		
		if( !$title ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'title is blank']);    
        }
		if( !$message ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'message is blank']);    
        }
		if( !$gameId ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'gameId is blank']);    
        }
		$this->result = $this->Login_model->sendNotificationSpecificAudience($title,$message,$gameId);
        $this->jsonOutput($this->result);
		
	}
	
	
	public function getAppState(){			
		$config = $this->config;			
		$crypto = $this->aescipher;			
		$json_obj = $this->readJson();		
        $this->result = $this->Login_model->getAppState();
		
		$data = $this->encrypt($crypto,$config,$this->result);
		
		$this->jsonOutput(['query' =>  $data]);       
	}
	
	
	private function encrypt($crypto,$config,$message){
		$message = json_encode($message);
		
		$data = $this->common->encryptCrossplatform($crypto,$config,$message);
		
		if($data['status']){			
			return $data['data'];
		}
		
	}
	
	private function decrypt($crypto,$config,$message){		
		$data = $this->common->decryptCrossplatform($crypto,$config,$message);
		if($data['status']){			
			return $data['data'];
		}
		
	}
	
	
	public function testEncrypt(){
		$config = $this->config;			
		$crypto = $this->aescipher;
		$json_obj = $this->readJson();		
		$data = $this->common->encryptCrossplatform($crypto,$config,json_encode($json_obj));
		if($data['status']){
			$this->jsonOutput(['query' =>  $data['data']]);			
		}
		
	}
	
	
	public function testDecrypt(){	
		$config = $this->config;			
		$crypto = $this->aescipher;
		$json_obj = $this->readJson();			
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'query is blank']); 
		}
		$data = $this->common->decryptCrossplatform($crypto,$config,$query);
		var_dump($data);
		if($data['status']){			
			return $data;
		}
		
	}
	
	
		
	
	
	
	
	
}