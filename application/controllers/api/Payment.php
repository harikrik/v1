<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Controller {
    
	private $result = '';
	private $validation = '';
	private $insert_a = [];

	public function __construct(){
        parent::__construct();
		header("Pragma: no-cache");
		header("Cache-Control: no-cache");
		header("Expires: 0");

        $this->load->library('Stack_web_gateway_paytm_kit');
		$this->load->model('Payment_model');		
    }
	

	/*public function createOrder(){		
		$json_obj = $this->readJson();		
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		$amount = isset($json_obj->amount) ? trim($json_obj->amount): '';		
		if( !$userId ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'user id is blank']);     
        }   
		if( !$amount ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'amount is blank']);     
        } 
        $this->result = $this->Payment_model->createOrder($userId,$amount); 		
        $this->jsonOutput($this->result);
	}*/
	
	
	public function createOrder(){
		$json_obj = $this->readJson();		
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		$amount = isset($json_obj->amount) ? trim($json_obj->amount): '';		
		if( !$userId ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'user id is blank']);     
        }   
		if( !$amount ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'amount is blank']);     
        } 
    	$this->db
    		->select('id as id,mobile as mobile,email as email')
    		->from('user');
		$userData = $this->db->get()->row();
		
		$isnert_a = ['user_id' => $userId, 'transaction_type' => 'cr', 'amount' =>  $amount];
    	$this->db->insert('transaction_master', $isnert_a);
    	$inserted_id = $this->db->insert_id();

    	if(!$inserted_id){		
    	
    		return (object)['status' => false, 'message' => 'Sorry, please try again later'];
    	}
		
		
		
		$orderId = $inserted_id;
		$customerId = $userData->id;
		$mobileNumber = $userData->mobile;
		$email = $userData->email;		
		$txnAmt = $amount;
		
		$industryTypeId = PAYTM_INDUSTRY_TYPE_ID;
		//$callBackUrl = 'http://13.233.107.147/index.php/api/payment-status';
		$callBackUrl = PAYTM_CALLBACK_URL.$orderId;
		//$checksumHash = $this->getChecksumHash($merchantId,$orderId,$customerId,$channelId,$txnAmt,$website,$industryTypeId);
		$checksumHash = $this->stack_web_gateway_paytm_kit->getChecksumFromArray($paytmParams, PAYTM_MERCHANT_KEY);
    	if($checksumHash){
			$data = array();
			$data["merchantId"] = PAYTM_MERCHANT_MID;
			$data["orderId"] = $orderId;
			$data["customerId"] = $customerId;
			$data["mobileNumber"] = $mobileNumber;
			$data["email"] = $email;
			$data["channelId"] = PAYTM_CHANNEL_ID;
			$data["txnAmt"] = $txnAmt;
			$data["website"] = PAYTM_MERCHANT_WEBSITE;
			$data["industryTypeId"] = PAYTM_INDUSTRY_TYPE_ID;
			$data["callBackUrl"] = $callBackUrl;
			$data["checksumHash"] = $checksumHash->data;
			
    		return (object)['status' => true, 'data' => $data];
    	}else{
    		return (object)['status' => false, 'message' => 'Sorry no user data found!'];
    	}
		
    }
	
	
	public function paymentStatus(){
		$data = $this->input->post();
		echo($data);	
		
	}
	
	


	
}
