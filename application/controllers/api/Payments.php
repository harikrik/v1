<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends CI_Controller {
//class Payments extends MY_Controller {
    public function __construct() {
        parent::__construct();
        //@session_start();

        //===================================================
        // Loads Paytm Authorized Files
        //===================================================
	//header("Pragma: no-cache");
	//header("Cache-Control: no-cache");
	//header("Expires: 0");
		$params[] = null;
		$this->load->library('aescipher',$params);
		$this->config->load('config');
        $this->load->library('stack_web_gateway_paytm_kit/Stack_web_gateway_paytm_kit');
	//===================================================
    }
    public function index()
    {
    }
    public function payby_paytm()
    {   date_default_timezone_set('Asia/Kolkata'); 
	
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);
    			
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		$amount = isset($json_obj->amount) ? trim($json_obj->amount): '';		
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);    
        }   
		if( !$amount ){
			$message = ['status'=> 'fail', 'message' => 'amount is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);
			     
        } 
    	$this->db
    		->select('id as id,mobile as mobile,email as email')
    		->from('user');
		$userData = $this->db->get()->row();
		$dateNow = date("Y-m-d H:i:s");
		
		
		$isnert_a = ['user_id' => $userId, 'transaction_type' => 'cr', 'amount' =>  $amount, 'created' =>  $dateNow];
    	$this->db->insert('transaction_master', $isnert_a);
    	$inserted_id = $this->db->insert_id();
       
    	if(!$inserted_id){		
    	
    		return (object)['status' => false, 'message' => 'Sorry, please try again later'];
    	}
		
			/*$paytmParams = array();
    		$paytmParams['ORDER_ID'] 		= $inserted_id;
    		$paytmParams['TXN_AMOUNT'] 		= $amount;
    		$paytmParams["CUST_ID"] 		= $userData->id;
    		$paytmParams["EMAIL"] 			= $userData->email;

		    $paytmParams["MID"] 			= PAYTM_MERCHANT_MID;
		    $paytmParams["CHANNEL_ID"] 		= PAYTM_CHANNEL_ID;
		    $paytmParams["WEBSITE"] 		= PAYTM_MERCHANT_WEBSITE;
		    
		    $paytmParams["INDUSTRY_TYPE_ID"]= PAYTM_INDUSTRY_TYPE_ID;*/
		
		$orderId = $inserted_id;
		$customerId = $userData->id;
		$mobileNumber = $userData->mobile;
		$email = $userData->email;		
		$txnAmt = $amount;
		//$paytmParams["CALLBACK_URL"] 	= PAYTM_CALLBACK_URL.$orderId;
		$industryTypeId = PAYTM_INDUSTRY_TYPE_ID;
		//$callBackUrl = "";
		//$callBackUrl = "http://13.233.107.147/index.php/api/payment-status";
		
		//$callBack = PAYTM_CALLBACK_URL.$orderId;
		$callBackUrl = PAYTM_CALLBACK_URL.$orderId;
		
		//$callBackUrl = str_replace('\\', '', $callBack);
		 
		//$checksumHash = $this->getChecksumHash($merchantId,$orderId,$customerId,$channelId,$txnAmt,$website,$industryTypeId);
		
		$paytmParams = array(	
	"MID" => PAYTM_MERCHANT_MID,
	"WEBSITE" => PAYTM_MERCHANT_WEBSITE,
	"INDUSTRY_TYPE_ID" => PAYTM_INDUSTRY_TYPE_ID, 
	"CHANNEL_ID" => PAYTM_CHANNEL_ID, 
	"ORDER_ID" => $orderId,
	"CUST_ID" => $customerId,
	"MOBILE_NO" => $mobileNumber,
	"EMAIL" => $email,    
	"TXN_AMOUNT" => $txnAmt, 	
	"CALLBACK_URL" => $callBackUrl,
);
		$checksumHash = $this->stack_web_gateway_paytm_kit->getChecksumFromArray($paytmParams, PAYTM_MERCHANT_KEY);
		
    	
			$data = array();
			$data["merchantId"] = PAYTM_MERCHANT_MID;
			$data["orderId"] = $orderId;
			$data["customerId"] = $customerId;
			$data["mobileNumber"] = $mobileNumber;
			$data["email"] = $email;
			$data["channelId"] = PAYTM_CHANNEL_ID;
			$data["txnAmt"] = $txnAmt;
			$data["website"] = PAYTM_MERCHANT_WEBSITE;
			$data["industryTypeId"] = PAYTM_INDUSTRY_TYPE_ID;
			$data["callBackUrl"] = $callBackUrl;
			$data["checksumHash"] = $checksumHash;
			//$abc = "123".$data["callBackUrl"]."456".$data["checksumHash"];
			$response = ['status'=> 'true', 'data' => $data];
			$data = $this->encrypt($crypto,$config,$response);						
			$this->jsonOutput(['query' =>  $data]);
			
    		//return (object)['status' => true, 'data' => $abc];
    		//return (object)['status' => true, 'data' => $this->jsonOutput($data)];
    	
    	
    }
	
	public function jsonOutput($data = array() ){
		$this->output->set_content_type('application/json');
		echo json_encode($data);
		exit;
	}
	
	public function readJson()
    {
       	$json_obj = json_decode('');
        $postdata = file_get_contents("php://input");
        $json_obj=json_decode($postdata);
        return $json_obj;
    }
	
	private function encrypt($crypto,$config,$message){
		$message = json_encode($message);
		$data = $this->common->encryptCrossplatform($crypto,$config,$message);
		if($data['status']){			
			return $data['data'];
		}
		
	}
	
	private function decrypt($crypto,$config,$message){		
		$data = $this->common->decryptCrossplatform($crypto,$config,$message);
		if($data['status']){			
			return $data['data'];
		}
		
	}


    public function paytm_response(){
    	$paytmChecksum 	= "";
		$paramList 		= array();
		$isValidChecksum= "FALSE";

		$paramList = $_POST;
		$paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg

		header("Pragma: no-cache");
		header("Cache-Control: no-cache");
		header("Expires: 0");
		log_message('info', $paramList["ORDERID"]);
		//log_message('info', $paramList["CHECKSUMHASH"]);
		/*foreach ($paramList as $item) {
			log_message('info', $item);
		}*/
		
		
		foreach($paramList as $key => $value) {
			log_message('info', $key." = ".$value);
		 // echo "$key is at $value";
		}
		
		
		//Verify all parameters received from Paytm pg to your application. Like MID received from paytm pg is same as your application’s MID, TXN_AMOUNT and ORDER_ID are same as what was sent by you to Paytm PG for initiating transaction etc.
		//$isValidChecksum = $this->stack_web_gateway_paytm_kit->verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.
		
		//$isValidChecksum = "TRUE";
		/*if($isValidChecksum == "TRUE") {
			log_message('info', '0012345');
			echo "<b>Checksum matched and following are the transaction details:</b>" . "<br/>";
			
			echo "<pre>";
			print_r($_POST);
			echo "<pre>";
			log_message('info', '01234');*/
			if ($_POST["STATUS"] == "TXN_SUCCESS") {
				
				//Process your transaction here as success transaction.
				//Verify amount & order id received from Payment gateway with your application's order id and amount.
				$orderId = $_POST["ORDERID"];
				$txnId = $_POST["TXNID"];
				$bankTxnAmt = $_POST["BANKTXNID"];
				$txnAmt = $_POST["TXNAMOUNT"];
				log_message('info', '1234');
				$this->db
					 ->select('user_id as userId,amount as amount')
    		         ->from('transaction_master')
    		         ->where('id',$orderId);
			    $userData =  $this->db->get()->row();    
				$userId = $userData->userId;
				$amt = $userData->amount;
				
				$this->db
					 ->select('coins as coins')
    		         ->from('user')
    		         ->where('id',$userId);
					 
				$userProfileData =  $this->db->get()->row(); 	 
				$balance = 	 $userProfileData->coins;					 
				$new_coins = (int)$amt + (int)$balance;
				$this->db
					->where('id', $userId)
					->set('coins', $new_coins)
					->update('user');
				$update_res = $this->db->affected_rows();
				if($update_res){	
				   $this->db
					->where('user_id', $userId)
					->where('id', $orderId)
					->set('status', '1')
					->set('paytm_txnid', $txnId)
					->set('bank_txnid', $bankTxnAmt)
					->update('transaction_master');
					$update_res_new = $this->db->affected_rows();
					if($update_res){
						$timelineData = array();
						$timelineData['userId'] = $userId;
						$timelineData['type'] = "cr";
						$timelineData['detail'] = "Deposit";
						$timelineData['amt'] = $amt;
						$this->common->addToTimeline($timelineData);
						echo "<b>Transaction status is success</b>" . "<br/>";
						
					}
				
				}
					//echo "<b>Transaction status is success</b>" . "<br/>";
					
					
					
				
				
			}
			else {
				echo "<b>Transaction status is failure</b>" . "<br/>";
			}

			if (isset($_POST) && count($_POST)>0 )
			{ 
				foreach($_POST as $paramName => $paramValue) {
						echo "<br/>" . $paramName . " = " . $paramValue;
				}
			}
			

		}
		/*else {
			echo "<b>Checksum mismatched.</b>";
			log_message('info', 'Checksum mismatched');
			//Process transaction as suspicious.
		}*/
    }
//}
?>
