<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	private $result = '';
	private $validation = '';
	private $insert_a = [];

	public function __construct(){
		header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		header("Access-Control-Allow-Methods: GET, OPTIONS");
		header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
        parent::__construct();
		
		$this->load->model('Admin_model');
		
    }
	
	
	public function login(){
		$json_obj = $this->readJson();
		$userName = isset($json_obj->userName) ? trim($json_obj->userName): '';
		$password = isset($json_obj->password) ? trim($json_obj->password): '';
		if( !$userName ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'userName is blank']);     
        }
		if( !$password ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'password is blank']);     
        }
        $this->result = $this->Admin_model->login($userName,$password);
        $this->jsonOutput($this->result);		
	}

	public function getLiveGames(){
		$json_obj = $this->readJson();
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';
		if( !$userId ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'userId is blank']);     
        }
        $this->result = $this->Admin_model->getLiveGames($userId);
        $this->jsonOutput($this->result);		
	}	
	
	
	
	public function createMatch(){
		$json_obj = $this->readJson();
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';
		
		$gameData = $json_obj->gameData;
		if( !$userId ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'userId is blank']);     
        }
        $this->result = $this->Admin_model->createMatch($userId,$gameData);
        $this->jsonOutput($this->result);		
	}
	
	public function getLiveGamesDetail(){
		$json_obj = $this->readJson();
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';
		$gameId = isset($json_obj->gameId) ? trim($json_obj->gameId): '';
		if( !$userId ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'userId is blank']);     
        }
		if( !$gameId ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'gameId is blank']);     
        }
        $this->result = $this->Admin_model->getLiveGamesDetail($userId,$gameId);
        $this->jsonOutput($this->result);		
	}

	public function updateMatchResult(){
		$json_obj = $this->readJson();
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';
		$gameId = isset($json_obj->gameId) ? trim($json_obj->gameId): '';
		$participantData = $json_obj->participantData;
		if( !$userId ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'userId is blank']);     
        }
		if( !$gameId ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'gameId is blank']);     
        }
        $this->result = $this->Admin_model->updateMatchResult($userId,$gameId,$participantData);
        $this->jsonOutput($this->result);		
	}
	
	public function cancelMatch(){
		$json_obj = $this->readJson();
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';
		$gameId = isset($json_obj->gameId) ? trim($json_obj->gameId): '';
		if( !$userId ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'userId is blank']);     
        }
		if( !$gameId ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'gameId is blank']);     
        }
        $this->result = $this->Admin_model->cancelMatchNew($userId,$gameId);
        $this->jsonOutput($this->result);		
	}

   
	
}
