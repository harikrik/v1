<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payout extends CI_Controller {

	private $result = '';
	private $validation = '';
	private $insert_a = [];

	public function __construct(){
        parent::__construct();
		$params[] = null;
		$this->load->library('aescipher',$params);
		$this->config->load('config');
		$this->load->model('Payout_model');
	   /* $clientId = "CF8626GPHQ9AH8JXUAUUQ";
		$clientSecret = "706d8b0fccda0e08a8fe4562fb85998fd9a50470";
		$stage = "TEST"; //use "PROD" for testing with live credentials
		$authParams["clientId"] = $clientId;
		$authParams["clientSecret"] = $clientSecret;
		$authParams["stage"] = $stage;
				
		$this->load->library('CfPayout',$authParams);*/
    }
	
	
	
	public function jsonOutput($data = array() ){
		$this->output->set_content_type('application/json');
		echo json_encode($data);
		exit;
	}
	
	public function readJson()
    {
       	$json_obj = json_decode('');
        $postdata = file_get_contents("php://input");
        $json_obj=json_decode($postdata);
        return $json_obj;
    }

	
	
	public function addBenificiery(){
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);	
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';
		$accountHolderName = isset($json_obj->accountHolderName) ? trim($json_obj->accountHolderName): '';
		$accountNumber = isset($json_obj->accountNumber) ? trim($json_obj->accountNumber): '';
		$ifsc = isset($json_obj->ifsc) ? trim($json_obj->ifsc): '';
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);         
        }
		if( !$accountHolderName ){
			$message = ['status'=> 'fail', 'message' => 'accountHolderName is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);     
			  
        }
		if( !$accountNumber ){
			$message = ['status'=> 'fail', 'message' => 'accountNumber is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);     
			  
        }
		if( !$ifsc ){
			$message = ['status'=> 'fail', 'message' => 'ifsc is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);     
			    
        }
	    $response = $this->Payout_model->addBenificiery($userId,$accountHolderName,$accountNumber,$ifsc);
		$data = $this->encrypt($crypto,$config,$response);						
		$this->jsonOutput(['query' =>  $data]);
		
	}	
	private function encrypt($crypto,$config,$message){
		$message = json_encode($message);
		$data = $this->common->encryptCrossplatform($crypto,$config,$message);
		if($data['status']){			
			return $data['data'];
		}
		
	}
	
	private function decrypt($crypto,$config,$message){		
		$data = $this->common->decryptCrossplatform($crypto,$config,$message);
		if($data['status']){			
			return $data['data'];
		}
		
	}
	
}
