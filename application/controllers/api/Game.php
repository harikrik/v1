<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Game extends MY_Controller   {
    
	private $result = '';
	private $validation = '';
	private $insert_a = [];

	public function __construct(){
        parent::__construct();
		$params[] = null;
		$this->load->library('aescipher',$params);
		$this->config->load('config');
		$this->load->model('Game_model');
		
    }
	
	
		public function withdrawFund(){		
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);		
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		$amount = isset($json_obj->amount) ? trim($json_obj->amount): '';		
		
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);     
        }    
		if( !$amount ){
			$message = ['status'=> 'fail', 'message' => 'amount is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		 
        $this->result = $this->Game_model->withdrawFund($userId,$amount); 		
        $data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
	}
	
	
	public function uploadFile(){		
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);		
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		$gameId = isset($json_obj->gameId) ? trim($json_obj->gameId): '';		
		$image = isset($json_obj->image) ? trim($json_obj->image): '';		
		
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);    
        }    
		if( !$gameId ){
			$message = ['status'=> 'fail', 'message' => 'gameId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			     
        }	
		if( !$image ){
			$message = ['status'=> 'fail', 'message' => 'image is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			 
        }		
        $this->result = $this->Game_model->uploadFile($userId,$gameId,$image); 		
        $data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
	}

	public function getLiveGames(){		
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);		
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);    
        }     
        $this->result = $this->Game_model->getLiveGames($userId); 		
        $data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
	}
	public function getLiveGamesByName(){
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);			
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		$gameName = isset($json_obj->gameName) ? trim($json_obj->gameName): '';		
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);     
        } 
		if( !$gameName ){
			$message = ['status'=> 'fail', 'message' => 'gameName is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);
			     
        } 
        $this->result = $this->Game_model->getLiveGamesByName($userId,$gameName);		
        $data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
		
	}
	
	
	public function getOngoingGames(){		
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);	
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);     
        }     
        $this->result = $this->Game_model->getOngoingGames($userId); 		
        $data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
	}
	
	public function updateParticipationStatus(){		
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);	
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		$gameId = isset($json_obj->gameId) ? trim($json_obj->gameId): '';		
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);    
        }     
		if( !$gameId ){
			$message = ['status'=> 'fail', 'message' => 'gameId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);
			    
        }     
        $this->result = $this->Game_model->updateParticipationStatus($userId,$gameId); 		
        $data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
	}	
	
	public function getGameResult(){		
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);				
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);     
        }     
        $this->result = $this->Game_model->getGameResult($userId); 		
        $data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
	}
	
	public function getGameResultByName(){		
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);		
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		$gameName = isset($json_obj->gameName) ? trim($json_obj->gameName): '';	
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);    
        }   
		if( !$gameName ){
			$message = ['status'=> 'fail', 'message' => 'gameName is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			     
        }   
        $this->result = $this->Game_model->getGameResultByName($userId,$gameName); 		
        $data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
	}
	
	public function getGameResultDetail(){		
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);		
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		$gameId = isset($json_obj->gameId) ? trim($json_obj->gameId): '';		
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);	   
        }  

		if( !$gameId ){
			$message = ['status'=> 'fail', 'message' => 'game id is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);	
			  
        } 		
        $this->result = $this->Game_model->getGameResultDetail($userId,$gameId); 		
        $data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
	}
	
	public function getUserCoins(){	
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		if( !$userId ){			
			$message = ['status'=> 'fail', 'message' => 'userId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);				
        }     
        $this->result = $this->Game_model->getUserCoins($userId); 
		$data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);       
	}
	
	
	
	
	public function getMatch(){		 
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);	
		$gameId = isset($json_obj->gameId) ? trim($json_obj->gameId): '';		
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';				
		if( !$gameId ){
			$message = ['status'=> 'fail', 'message' => 'game id is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			    
        }   
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'user id is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			  
        }  		
        //$this->result = $this->Game_model->getMatch($gameId,$userId); 		
		$this->result = $this->Game_model->getMatchNew($gameId,$userId);
        $data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
	}
	
	public function getNotification(){		 
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);			
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'user id is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			     
        }     
        $this->result = $this->Game_model->getNotification($userId); 		
        $data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
	}
	
	public function addParticipant(){		 
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);	
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';	
		$pubgName = isset($json_obj->pubgName) ? trim($json_obj->pubgName): '';	
		$gameId = isset($json_obj->gameId) ? trim($json_obj->gameId): '';	
		$uniqueId = isset($json_obj->uniqueId) ? trim($json_obj->uniqueId): '';	
		//echo($uniqueId."  *****");
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'user id is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);    
        }     
		if( !$pubgName ){
			$message = ['status'=> 'fail', 'message' => 'Pubg name is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			
        }   
		if( !$gameId ){
			$message = ['status'=> 'fail', 'message' => 'gameId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        } 
		/*if( !$uniqueId ){
			$this->jsonOutput(['status'=> 'fail', 'message' => 'uniqueId is blank']);     
        } */
        $this->result = $this->Game_model->addParticipant($userId,$pubgName,$gameId,$uniqueId); 		
       	$data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
	}
	
	
	public function removeAllParticipants(){
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);		
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';			
		$gameId = isset($json_obj->gameId) ? trim($json_obj->gameId): '';	
		
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'game id is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }     
	
		if( !$gameId ){
			$message = ['status'=> 'fail', 'message' => 'gameId is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);
			    
        }   
        $this->result = $this->Game_model->removeAllParticipants($userId,$gameId); 		
       	$data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
		
	}
	
	
	public function getCoinList(){		
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);	
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'game id is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);     
        }     
        $this->result = $this->Game_model->getCoinList($userId); 		
        $data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
	}


	public function getBenificiaryStatus(){		
		$config = $this->config;			
		$crypto = $this->aescipher;		
		$json_obj = $this->readJson();
		$query = isset($json_obj->query) ? trim($json_obj->query): '';
		if( !$query ){			
			$message = ['status'=> 'fail', 'message' => 'query is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);			   
        }		
		$json_obj = $this->decrypt($crypto,$config,$query);	
		$json_obj = json_decode($json_obj);	
		$userId = isset($json_obj->userId) ? trim($json_obj->userId): '';		
		if( !$userId ){
			$message = ['status'=> 'fail', 'message' => 'game id is blank'];
			$data = $this->encrypt($crypto,$config,$message);			
			$this->jsonOutput(['query' =>  $data]);     
        }     
        $this->result = $this->Game_model->getBenificiaryStatus($userId); 		
        $data = $this->encrypt($crypto,$config,$this->result);						
		$this->jsonOutput(['query' =>  $data]);
	}
	
	
	private function encrypt($crypto,$config,$message){
		$message = json_encode($message);
		$data = $this->common->encryptCrossplatform($crypto,$config,$message);
		if($data['status']){			
			return $data['data'];
		}
		
	}
	
	private function decrypt($crypto,$config,$message){		
		$data = $this->common->decryptCrossplatform($crypto,$config,$message);
		if($data['status']){			
			return $data['data'];
		}
		
	}

	
}
