<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends MY_Controller   {
    
	private $result = '';
	private $validation = '';
	private $insert_a = [];

	public function __construct(){
        parent::__construct();
		$params[] = null;
		
		//$this->config->load('config');
		$this->load->helper('download');
		//$this->load->model('Game_model');
		
    }
	
		/*public function download($fileName = NULL) {  
			$fileName = "app-release.apk";
			$file = realpath ( "/var/www/html" ) ."/". $fileName;
			echo($file);
			// check file exists    
			if (file_exists ( $file )) {
				echo($file);
			 // get file content
			 $data = file_get_contents ( $file );
			 //force download
			 //$this->output->set_content_type('application/vnd.android.package-archive');
			 //header('Content-Type: application/vnd.android.package-archive');
			 //header('Content-Disposition: attachment; filename="app-release.apk"');
			 //$this->output->set_content_disposition('inline');
		    force_download( $fileName, $data );
			// force_download ( $file, null,true );
			 
			}	
		
	}*/
	
	
	public function download($fileName=NULL){
		$fileName="app-release.apk";
		$file=realpath("/var/www/html")."/".$fileName;
		if(file_exists($file)){
			ob_start();
			// $data=file_get_contents($file);
			header("Content-Type:application/vnd.android.package-archive");
			header("Content-Disposition:attachment;filename=\"".basename($file)."\"");
			header("Content-Transfer-Encoding:binary");
			header("Expires:0");
			header("Cache-Control:must-revalidate,post-check=0,pre-check=0");
			header("Pragma:public");
			header("Content-Length:".filesize($file));
			ob_clean();
			flush();
			readfile($file);
			exit;
		}
	}
	
	
		
	
	
	

	
}
